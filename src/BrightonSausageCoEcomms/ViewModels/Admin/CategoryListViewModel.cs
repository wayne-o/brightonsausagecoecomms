using System.Collections.Generic;
using BrightonSausageCoEcomms.Infrastructure.Models;

namespace BrightonSausageCoEcomms.ViewModels.Admin
{
    public class CategoryListViewModel
    {
        public CategoryListViewModel(IList<Category> categories)
        {
            Categories = categories;
        }

        public IList<Category> Categories { get; set; }
    }
}