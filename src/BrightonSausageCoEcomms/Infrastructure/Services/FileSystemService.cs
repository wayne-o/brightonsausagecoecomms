﻿using System;
using System.IO;

namespace BrightonSausageCoEcomms.Infrastructure.Services
{
    public class FileSystemService : IFileSystemService
    {
        private readonly string storagePath;

        public FileSystemService(string storagePath)
        {
            this.storagePath = storagePath;
        }

        public void CreateFolder(string path)
        {
            var directoryInfo = new DirectoryInfo(path);
            if (directoryInfo.Exists)
            {
                throw new ArgumentException("Directory {0} already exists".With(path));
            }

            Directory.CreateDirectory(directoryInfo.FullName);
        }

        public bool FolderExists(string path)
        {
            return Directory.Exists(path);
        }

        public void DeleteFile(string path)
        {
            var fileInfo = new FileInfo(path);

            if (!fileInfo.Exists)
            {
                throw new ArgumentException("File {0} does not exist".With(path));
            }

            fileInfo.Delete();
        }
    }
}