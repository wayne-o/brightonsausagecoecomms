using System.ComponentModel.DataAnnotations;

namespace BrightonSausageCoEcomms.ViewModels.Admin
{
    public class CategoryViewModel
    {
        [Required]
        public string CategoryName { get; set; }

        public bool Enabled { get; set; }

        public string Slug { get; set; }

        public string Id { get; set; }

        public bool Featured { get; set; }
    }
}