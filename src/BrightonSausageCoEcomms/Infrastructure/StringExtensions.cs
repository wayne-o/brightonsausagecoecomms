﻿using System.Globalization;

namespace BrightonSausageCoEcomms.Infrastructure
{
    public static class StringExtensions
    {
        public static string With(this string format, params object[] args)
        {
            return string.Format(CultureInfo.CurrentUICulture, format, args);
        }
    }
}