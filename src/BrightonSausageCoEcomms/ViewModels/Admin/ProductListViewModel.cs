using System.Collections.Generic;
using BrightonSausageCoEcomms.Infrastructure.Models;

namespace BrightonSausageCoEcomms.ViewModels.Admin
{
    public class ProductListViewModel
    {
        public ProductListViewModel(IList<Product> products)
        {
            Products = products;
        }

        public IList<Product> Products { get; set; }
    }
}