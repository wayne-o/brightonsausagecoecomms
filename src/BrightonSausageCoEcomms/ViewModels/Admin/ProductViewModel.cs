using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;

namespace BrightonSausageCoEcomms.ViewModels.Admin
{
    public class ProductViewModel
    {
        [Required]
        public string ProductName { get; set; }

        [Required]
        public string ProductDescription { get; set; }

        [Required]
        public string ProductShortDescription { get; set; }

        [Required]
        public decimal Price { get; set; }

        public bool Published { get; set; }

        public bool Enabled { get; set; }

        public string Slug { get; set; }

        public string Id { get; set; }

        public string ProductImage { get; set; }

        public List<CategoryInfo> Categories { get; set; }

        public List<ItemViewModel> AvalailableCategories { get; set; }
    }
}