namespace BrightonSausageCoEcomms.Infrastructure.Models
{
    public class Category
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool Enabled { get; set; }

        public string Slug { get; set; }

        public bool Featured { get; set; }
    }
}