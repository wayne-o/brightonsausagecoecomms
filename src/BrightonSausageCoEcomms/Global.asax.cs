﻿using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Installers;
using BrightonSausageCoEcomms.Infrastructure.Repository;
using BrightonSausageCoEcomms.Infrastructure.Services;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace BrightonSausageCoEcomms
{
    public class MvcApplication : HttpApplication
    {
        private readonly IWindsorContainer container;

        public MvcApplication()
        {
            this.container = new WindsorContainer();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            container.Register(Component.For<IControllerFactory>().ImplementedBy<DefaultControllerFactory>());

            container.Register(AllTypes.FromAssembly(System.Reflection.Assembly.GetExecutingAssembly()).BasedOn<IController>().LifestyleTransient());

            container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)));

            container.Install(new MongoInstaller());

            container.Register(Component.For<IImageUploadService>().ImplementedBy<ImageUploadService>());
            container
                .Register(Component.For<IFileSystemService>()
                                   .ImplementedBy<FileSystemService>()
                                   .DependsOn(new Hashtable 
                                    { 
                                        { "storagePath", ConfigurationManager.AppSettings["ImagesUpload"] }
                                    }));


            container.Register(Component.For<IMediaSettings>().ImplementedBy<MediaSettings>());

            DependencyResolver.SetResolver(new WindsorDependencyResolver(container));
        }

        public static void Start()
        {
        }

        public override void Dispose()
        {
            this.container.Dispose();
            base.Dispose();
        }
    }
}