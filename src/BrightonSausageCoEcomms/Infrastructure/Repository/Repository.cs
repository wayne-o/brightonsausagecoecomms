using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace BrightonSausageCoEcomms.Infrastructure.Repository
{
    public class Repository<T> : IRepository<T>
    {
        private readonly MongoCollection<T> docs;

        public Repository(MongoCollection<T> docs)
        {
            this.docs = docs;
        }

        public IList<T> GetAll()
        {
            return docs.FindAll().Select<T, T>(x => x.As<T>()).ToList();
        }

        public void Save(T doc)
        {
            docs.Save(doc);
        }

        public void Save(IEnumerable<T> docsToSave)
        {
            foreach (var doc in docsToSave) Save(doc);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            var query = Query.EQ("_id", id);
            docs.Remove(query);
        }
    }
}