﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MongoDB.Driver;

namespace BrightonSausageCoEcomms.Infrastructure.Installers
{
    public class MongoInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Register(
                          Component.For<MongoServer>()
                              .UsingFactoryMethod(k => MongoServer.Create(MongoUri()))
                              .OnCreate((k, s) => s.Connect())
                              .LifeStyle.Singleton,

                          Component.For<MongoDatabase>().UsingFactoryMethod(GetMongoDatabase),

                          Component.For<ILazyComponentLoader>()
                              .ImplementedBy<MongoCollectionComponentLoader>()
                              .LifeStyle.Singleton);
        }

        Uri MongoUri()
        {
            return new Uri(ConfigurationManager.AppSettings["MONGODB_URL"]);
        }

        MongoDatabase GetMongoDatabase(IKernel kernel)
        {
            return kernel.Resolve<MongoServer>().GetDatabase(ConfigurationManager.AppSettings["MONGODB_NAME"]);
        }
    }
}