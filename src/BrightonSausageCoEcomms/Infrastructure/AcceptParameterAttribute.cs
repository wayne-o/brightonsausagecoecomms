using System;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicValidationHelper;

namespace BrightonSausageCoEcomms.Infrastructure
{
    public class InjectIndexParameterAttribute : ActionFilterAttribute
    {
        public string TargetArgument { get; set; }

        /// <summary>
        /// Gets or sets the value to use in submit button to identify which method to select. This must be unique in each controller.
        /// </summary>
        public string Action { get { return this.ActionRegex.Split(':')[0]; } }

        /// <summary>
        /// Gets or sets the regular expression to match the action.
        /// </summary>
        public string ActionRegex { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            var req = filterContext.HttpContext.Request.Form.AllKeys.Any() ? filterContext.HttpContext.Request.Form : filterContext.HttpContext.Request.QueryString;

            if (!string.IsNullOrEmpty(this.ActionRegex))
            {
                foreach (
                    var key in
                        req.AllKeys.Where(
                            k => k.StartsWith(Action, true, System.Threading.Thread.CurrentThread.CurrentCulture)))
                {
                    if (key.Contains(":"))
                    {
                        if (key.Split(':').Count() == this.ActionRegex.Split(':').Count())
                        {
                            bool match = false;
                            for (int i = 0; i < key.Split(':').Count(); i++)
                            {
                                if (Regex.IsMatch(key.Split(':')[0], this.ActionRegex.Split(':')[0]))
                                {

                                    filterContext.ActionParameters[TargetArgument] = key.Split(':')[1];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public class AcceptParameterAttribute : ActionMethodSelectorAttribute
    {
        public string Action { get; set; }

        public string ActionRegex { get; set; }

        public string TargetArgument { get; set; }

        public override bool IsValidForRequest(ControllerContext controllerContext,
                                               MethodInfo methodInfo)
        {
            if (controllerContext == null)
            {
                throw new ArgumentNullException("controllerContext");
            }

            Func<NameValueCollection> formGetter;
            Func<NameValueCollection> queryStringGetter;

            ValidationUtility.GetUnvalidatedCollections(HttpContext.Current, out formGetter, out queryStringGetter);

            NameValueCollection form = formGetter();
            NameValueCollection queryString = queryStringGetter();

            NameValueCollection req = form.AllKeys.Any() ? form : queryString;

            if (!string.IsNullOrEmpty(ActionRegex))
            {
                foreach (string key in
                    req.AllKeys.Where(k => k.StartsWith(Action, true, Thread.CurrentThread.CurrentCulture)))
                {
                    if (key.Contains(":"))
                    {
                        if (key.Split(':').Count() == ActionRegex.Split(':').Count())
                        {
                            bool match = false;
                            for (int i = 0; i < key.Split(':').Count(); i++)
                            {
                                if (Regex.IsMatch(key.Split(':')[0], ActionRegex.Split(':')[0]))
                                {
                                    match = true;
                                }
                                else
                                {
                                    match = false;
                                    break;
                                }
                            }

                            if (match)
                            {
                                return !string.IsNullOrEmpty(req[key]);
                            }
                        }
                    }
                    else
                    {
                        if (Regex.IsMatch(key, Action + ActionRegex))
                        {
                            return !string.IsNullOrEmpty(req[key]);
                        }
                    }
                }

                return false;
            }
            else
            {
                return req.AllKeys.Contains(Action);
            }
        }
    }
}