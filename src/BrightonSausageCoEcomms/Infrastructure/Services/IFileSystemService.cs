﻿namespace BrightonSausageCoEcomms.Infrastructure.Services
{
    public interface IFileSystemService
    {
        void CreateFolder(string path);
        bool FolderExists(string path);
        void DeleteFile(string combine);
    }
}