using System;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Repository;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Coypu;
using TechTalk.SpecFlow;

namespace BrightonSausageCoEcommsBehaviors.Specs
{
    public class StepsBase
    {
        public static IWindsorContainer WindsorContainer
        {
            get
            {
                if (FeatureContext.Current.ContainsKey("Container"))
                {
                    return (IWindsorContainer)FeatureContext.Current["Container"];
                }

                var container = new WindsorContainer();
                container.Install(new BrightonSausageCoEcomms.Infrastructure.Installers.MongoInstaller());
                container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)));
                FeatureContext.Current["Container"] = container;

                return container;
            }
        }

        public static BrowserSession BrowserSession
        {
            get
            {
                if (FeatureContext.Current.ContainsKey("BrowserSession"))
                {
                    return (BrowserSession)FeatureContext.Current["BrowserSession"];
                }

                var sessionConfiguration = new SessionConfiguration
                                               {
                                                   Timeout = TimeSpan.FromMilliseconds(1000)
                                               };

                var browserSession = new BrowserSession(sessionConfiguration);
                FeatureContext.Current.Add("BrowserSession", browserSession);
                return browserSession;
            }
        }
    }
}