﻿Feature: Viewing Products Page
	In order view the websites home page
	As a standard user
	I want to be able to browse and purhcase products on the home page

Scenario: Viewing products in the default view
	Given the following categories exist
		| Id		| Name		| Enabled |
		| product1  | pork		| true    |
		| product2	| sausage   | true    |
		And the following products exist
		| Name      | Description      | Price | Published | Categories    | Enabled |
		| sausage 1 | Atlas Shrugged   | 25.04 | true      | pork, sausage | true    |
		| sausage 2 | The Fountainhead | 20.15 | true      | pork, sausage | true    |
		And I have navigated to the Products page
	Then I expect to see the featured products listed

Scenario: Viewing a products details page
	Given the following categories exist
		| Id		| Name		| Enabled |
		| product1  | pork		| true    |
		| product2	| sausage   | true    |
		And the following products exist
		| Name      | Description      | Price | Published | Categories    | Enabled |
		| sausage 1 | Atlas Shrugged   | 25.04 | true      | pork, sausage | true    |
		| sausage 2 | The Fountainhead | 20.15 | true      | pork, sausage | true    |
		And I have navigated to the Products page
	When I click the "sausage-1" link
	Then I expect to see the product detail page for "sausage 1"