﻿using System;
using System.Collections.Generic;
using System.Web;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Services;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace BrightonSausageCoEcommsBehaviors.Services
{
    [TestFixture]
    public class UploadServiceBehaviors
    {
        [Test]
        public void When_uploading_image_and_image_directory_does_not_exist_it_is_created()
        {
            const string path = "some/relative/path/";
            var fileServiceMock = new Mock<IFileSystemService>();
            var mediaSettings = new Mock<IMediaSettings>();
            var blacklistExtensions = new List<string> {"disalowed"};
            mediaSettings.Setup(x => x.ExtensionBlackList).Returns(blacklistExtensions);

            var file = new Mock<HttpPostedFileBase>();
            file.Setup(x => x.FileName).Returns("file.allowed").Verifiable();
            file.Setup(x => x.SaveAs(It.IsAny<string>())).Verifiable();

            var service = new ImageUploadService(fileServiceMock.Object, mediaSettings.Object);

            fileServiceMock.Setup(x => x.FolderExists(It.IsAny<string>())).Returns(false);

            service.UploadMediaFile(path, file.Object);

            fileServiceMock.Verify(x => x.FolderExists(It.IsAny<string>()), Times.Once());
            fileServiceMock.Verify(x => x.CreateFolder(It.IsAny<string>()), Times.Once());

            file.VerifyAll();
        }

        [Test]
        public void When_uploading_image_which_does_have_a_blacklisted_extension_exception_is_thrown()
        {
            Action test = () =>
                              {
                                  const string path = "some/path/image.disalowed";
                                  var mediaSettings = new Mock<IMediaSettings>();
                                  var fileService = new Mock<IFileSystemService>();

                                  var blacklistExtensions = new List<string> {"disalowed"};
                                  mediaSettings.Setup(x => x.ExtensionBlackList).Returns(blacklistExtensions);

                                  var service = new ImageUploadService(fileService.Object, mediaSettings.Object);
                                  service.UploadMediaFile(path, null);
                              };

            test.ShouldThrow<FileExtensionBlacklistValidationException>();
        }

        [Test]
        public void When_uploading_image_which_does_not_have_a_blacklisted_extension_exception_is_not_thrown()
        {
            Action test = () =>
                              {
                                  const string path = "some/path/image.alowed";
                                  var mediaSettings = new Mock<IMediaSettings>();
                                  var fileService = new Mock<IFileSystemService>();

                                  var blacklistExtensions = new List<string> {"disalowed"};
                                  mediaSettings.Setup(x => x.ExtensionBlackList).Returns(blacklistExtensions);

                                  var service = new ImageUploadService(fileService.Object, mediaSettings.Object);
                                  service.UploadMediaFile(path, null);
                              };

            test.ShouldNotThrow<FileExtensionBlacklistValidationException>();
        }

        [Test]
        public void When_uploading_image_with_blacklisted_extension_exception_is_thrown()
        {
            Action test = () =>
                              {
                                  const string path = "some/path/image.disalowed";
                                  var mediaSettings = new Mock<IMediaSettings>();
                                  var fileService = new Mock<IFileSystemService>();

                                  var blacklistExtensions = new List<string> {"disalowed"};
                                  mediaSettings.Setup(x => x.ExtensionBlackList).Returns(blacklistExtensions);

                                  var service = new ImageUploadService(fileService.Object, mediaSettings.Object);
                                  service.UploadMediaFile(path, null);
                              };

            test.ShouldThrow<FileExtensionBlacklistValidationException>();
        }
    }
}