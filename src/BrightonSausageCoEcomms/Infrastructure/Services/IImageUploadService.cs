﻿using System.Web;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;

namespace BrightonSausageCoEcomms.Infrastructure.Services
{
    public interface IImageUploadService
    {
        ImageInfo UploadMediaFile(string storagePath, HttpPostedFileBase file);

        void DeleteMediaFile(string folderPath, string fileName);
    }
}