﻿using System.Collections.Generic;

namespace BrightonSausageCoEcomms.Infrastructure.Services
{
    public interface IMediaSettings
    {
        List<string> ExtensionBlackList { get; }
    }
}