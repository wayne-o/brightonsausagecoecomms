<h1>Building the www.brightonsausageco.com website - 31/10/2012</h1>

<p>Session 6 on the BrightonSausageCo.com website and I'm nearly done with the first scenario! I left off last night with the ability to add categories but the 1st scenario wasn't quite complete. To get that passing I have quickly boshed out a table to list the existing categories in so that I can list more than just the name of the category. I have also populated the final specflow step so that it can look to see if the category we just added is enabled. Here's the respective code for that:</p>

<script src="https://gist.github.com/3989099.js?file=gistfile1.html"></script>

<script src="https://gist.github.com/3989103.js?file=gistfile1.cs"></script>

<p>Next up I want to add some scenarios that harden what we've just built. By this I mean we might want to add some validation constraints to make sure we have some value for the category name. We might want to check to see if the category already exists. And then I'll start thinking about editing the existing categories. Big list for one night! Here's the feature file with the initial spin off of that lot:</p>

<script src="https://gist.github.com/3989171.js?file=gistfile1.feature"></script>




