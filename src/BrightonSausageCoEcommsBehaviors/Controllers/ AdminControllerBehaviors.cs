﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BrightonSausageCoEcomms.Controllers;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;
using BrightonSausageCoEcomms.Infrastructure.Services;
using BrightonSausageCoEcomms.ViewModels;
using BrightonSausageCoEcomms.ViewModels.Admin;
using FluentAssertions;
using Moq;
using MvcContrib.TestHelper;
using NUnit.Framework;

namespace BrightonSausageCoEcommsBehaviors.Controllers
{
    [TestFixture]
    public class AdminControllerBehaviors
    {
        [Test]
        public void When_requesting_the_admin_page_result_is_correct()
        {
            const string expectedViewName = "Index";
            var categoryRepository = new Mock<IRepository<Category>>();
            var productRepository = new Mock<IRepository<Product>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);
            var result = controller.Index();
            result.Should().BeAssignableTo<ActionResult>();
            ((ViewResult)result).ViewName.Should().Be(expectedViewName);
        }

        [Test]
        public void When_requesting_the_admin_categories_page_result_is_correct()
        {
            const int expectedCategoryCount = 3;
            const string expectedViewName = "Category";
            var categoryRepository = new Mock<IRepository<Category>>();
            var productRepository = new Mock<IRepository<Product>>();

            categoryRepository.Setup(x => x.GetAll()).Returns(new List<Category>
                                                          {
                                                              new Category(),
                                                              new Category(),
                                                              new Category()
                                                          });

            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);

            var result = controller.Category();
            result.Should().BeAssignableTo<ActionResult>();
            result.AssertViewRendered().ForView(expectedViewName);
            result.Model.Should().BeAssignableTo<CategoryListViewModel>();

            ((CategoryListViewModel)result.Model).Categories.Count.Should().Be(expectedCategoryCount);
        }

        [Test]
        public void When_requesting_the_admin_categories_edit_page_result_is_correct()
        {
            const int expectedCategoryCount = 3;
            const string expectedViewName = "Category";
            var categoryRepository = new Mock<IRepository<Category>>();
            var productRepository = new Mock<IRepository<Product>>();

            categoryRepository.Setup(x => x.GetAll()).Returns(new List<Category>
                                                          {
                                                              new Category
                                                                  {
                                                                      Id = "1"
                                                                  },
                                                              new Category
                                                                  {
                                                                      Id = "2"
                                                                  },
                                                              new Category
                                                                  {
                                                                      Id = "3"
                                                                  }
                                                          });

            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);

            var result = controller.CategoryEdit("2");
            result.AssertViewRendered().ForView("CategoryEdit");

            ((ViewResult)result).Model.Should().BeOfType<CategoryViewModel>();
        }

        [Test]
        public void When_requesting_the_admin_categories_add_page_result_is_correct()
        {
            const string expectedViewName = "CategoryAdd";
            var categoryRepository = new Mock<IRepository<Category>>();
            var productRepository = new Mock<IRepository<Product>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);
            var result = controller.CategoryAdd();
            result.Should().BeAssignableTo<ActionResult>();
            ((ViewResult)result).ViewName.Should().Be(expectedViewName);
        }

        [Test]
        public void When_posting_to_the_admin_categories_add_page_result_is_correct()
        {
            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetAll()).Returns(new List<Category>
                                                          {
                                                              new Category
                                                                  {
                                                                    Name = "test 99"
                                                                  },
                                                              new Category
                                                                  {
                                                                    Name = "test 98"
                                                                  },
                                                              new Category
                                                                  {
                                                                    Name = "test 97"
                                                                  }
                                                          });

            categoryRepository.Setup(x => x.Save(It.IsAny<Category>())).Verifiable();
            var productRepository = new Mock<IRepository<Product>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);
            var viewModel = new CategoryViewModel
                                {
                                    CategoryName = "test"
                                };
            var result = controller.CategoryAdd(viewModel);
            result.Should().BeAssignableTo<RedirectToRouteResult>();
            ((RedirectToRouteResult)result).RouteValues["Action"].Should().Be("Category");
            ((RedirectToRouteResult)result).RouteValues["Controller"].Should().Be("Admin");

            categoryRepository.VerifyAll();
        }

        [Test]
        public void When_posting_to_the_admin_categories_add_page_with_duplicate_category_name_result_is_correct()
        {
            const string expectedViewName = "CategoryAdd";
            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetAll()).Returns(new List<Category>
                                                          {
                                                              new Category
                                                                  {
                                                                    Name = "test 99"
                                                                  },
                                                              new Category
                                                                  {
                                                                    Name = "test 98"
                                                                  },
                                                              new Category
                                                                  {
                                                                    Name = "test"
                                                                  }
                                                          });

            categoryRepository.Setup(x => x.Save(It.IsAny<Category>())).Verifiable();
            var productRepository = new Mock<IRepository<Product>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);

            var viewModel = new CategoryViewModel
            {
                CategoryName = "test"
            };

            var result = controller.CategoryAdd(viewModel);
            result.Should().BeAssignableTo<ActionResult>();
            ((ViewResult)result).ViewName.Should().Be(expectedViewName);

            categoryRepository.Verify(x => x.Save(It.IsAny<Category>()), Times.Never());
        }

        [Test]
        public void When_posting_to_the_admin_categories_edit_page_result_is_correct()
        {
            var categoryRepository = new Mock<IRepository<Category>>();

            categoryRepository.Setup(x => x.Save(It.IsAny<Category>())).Verifiable();
            categoryRepository.Setup(x => x.GetAll()).Returns(new List<Category>
                                                          {
                                                              new Category
                                                                  {
                                                                    Name = "test 99"
                                                                  },
                                                              new Category
                                                                  {
                                                                    Name = "test 98"
                                                                  },
                                                              new Category
                                                                  {
                                                                    Name = "test 97"
                                                                  }
                                                          });

            var productRepository = new Mock<IRepository<Product>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);
            var viewModel = new CategoryViewModel
            {
                CategoryName = "test"
            };

            var result = controller.CategoryEdit(viewModel);
            result.Should().BeAssignableTo<RedirectToRouteResult>();
            ((RedirectToRouteResult)result).RouteValues["Action"].Should().Be("Category");
            ((RedirectToRouteResult)result).RouteValues["Controller"].Should().Be("Admin");

            categoryRepository.VerifyAll();
        }

        [Test]
        public void When_posting_to_the_admin_categories_edit_page_with_same_name_result_is_correct()
        {
            var categoryRepository = new Mock<IRepository<Category>>();

            categoryRepository.Setup(x => x.Save(It.IsAny<Category>())).Verifiable();
            categoryRepository.Setup(x => x.GetAll()).Returns(new List<Category>
                                                          {
                                                              new Category
                                                                  {
                                                                    Name = "test 99"
                                                                  },
                                                              new Category
                                                                  {
                                                                    Name = "test 98"
                                                                  },
                                                              new Category
                                                                  {
                                                                    Name = "test"
                                                                  }
                                                          });

            var productRepository = new Mock<IRepository<Product>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);
            var viewModel = new CategoryViewModel
            {
                CategoryName = "test"
            };

            var result = controller.CategoryEdit(viewModel);
            result.Should().BeAssignableTo<RedirectToRouteResult>();
            ((RedirectToRouteResult)result).RouteValues["Action"].Should().Be("Category");
            ((RedirectToRouteResult)result).RouteValues["Controller"].Should().Be("Admin");

            categoryRepository.VerifyAll();
        }

        [Test]
        public void When_requesting_the_admin_product_page_result_is_correct()
        {
            const int expectedProductCount = 3;
            const string expectedViewName = "Product";
            var productRepository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();
            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                      Name = "test"
                                                                  },
                                                              new Product{
                                                                      Name = "test"
                                                                  },
                                                              new Product{
                                                                      Name = "test"
                                                                  }
                                                          });

            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);

            var result = controller.Product();
            result.Should().BeAssignableTo<ActionResult>();
            result.AssertViewRendered().ForView(expectedViewName);
            result.Model.Should().BeAssignableTo<ProductListViewModel>();

            ((ProductListViewModel)result.Model).Products.Count.Should().Be(expectedProductCount);
        }

        [Test]
        public void When_posting_to_the_admin_product_page_result_is_correct()
        {
            var productRepository = new Mock<IRepository<Product>>();

            productRepository.Setup(x => x.Save(It.IsAny<Product>())).Verifiable();
            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                    Name = "test 99"
                                                                  },
                                                              new Product
                                                                  {
                                                                    Name = "test 98"
                                                                  },
                                                              new Product
                                                                  {
                                                                    Name = "test 97"
                                                                  }
                                                          });

            var categoryRepository = new Mock<IRepository<Category>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);
            var viewModel = new ProductViewModel()
            {
                ProductName = "test"
            };

            var result = controller.ProductAdd(viewModel);
            result.Should().BeAssignableTo<RedirectToRouteResult>();
            ((RedirectToRouteResult)result).RouteValues["Action"].Should().Be("Product");
            ((RedirectToRouteResult)result).RouteValues["Controller"].Should().Be("Admin");

            productRepository.VerifyAll();
        }

        [Test]
        public void When_posting_to_the_admin_products_edit_page_result_is_correct()
        {
            var productRepository = new Mock<IRepository<Product>>();

            productRepository.Setup(x => x.Save(It.IsAny<Product>())).Verifiable();
            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                    Name = "test 99"
                                                                  },
                                                              new Product
                                                                  {
                                                                    Name = "test 98"
                                                                  },
                                                              new Product
                                                                  {
                                                                    Name = "test 97"
                                                                  }
                                                          });

            var categoryRepository = new Mock<IRepository<Category>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);
            var viewModel = new ProductViewModel
            {
                ProductName = "test"
            };

            var result = controller.ProductEdit(viewModel);
            result.Should().BeAssignableTo<RedirectToRouteResult>();
            ((RedirectToRouteResult)result).RouteValues["Action"].Should().Be("Product");
            ((RedirectToRouteResult)result).RouteValues["Controller"].Should().Be("Admin");

            productRepository.VerifyAll();
        }

        [Test]
        public void When_requesting_the_admin_product_edit_page_result_is_correct()
        {
            const int expectedCategoryCount = 3;
            const string expectedViewName = "Category";
            var categoryRepository = new Mock<IRepository<Category>>();
            var productRepository = new Mock<IRepository<Product>>();

            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                      Id = "1"
                                                                  },
                                                              new Product
                                                                  {
                                                                      Id = "2"
                                                                  },
                                                              new Product
                                                                  {
                                                                      Id = "3"
                                                                  }
                                                          });

            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);

            var result = controller.ProductEdit("2");
            result.AssertViewRendered().ForView("ProductEdit");

            ((ViewResult)result).Model.Should().BeOfType<ProductViewModel>();
        }

        [Test]
        public void When_posting_to_the_admin_product_edit_page_result_is_correct()
        {
            var productRepository = new Mock<IRepository<Product>>();

            productRepository.Setup(x => x.Save(It.IsAny<Product>())).Verifiable();
            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                    Name = "test 99"
                                                                  },
                                                              new Product
                                                                  {
                                                                    Name = "test 98"
                                                                  },
                                                              new Product
                                                                  {
                                                                    Name = "test 97"
                                                                  }
                                                          });

            var categoryRepository = new Mock<IRepository<Category>>();
            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);
            var viewModel = new ProductViewModel
            {
                ProductName = "test"
            };

            var result = controller.ProductEdit(viewModel);
            result.Should().BeAssignableTo<RedirectToRouteResult>();
            ((RedirectToRouteResult)result).RouteValues["Action"].Should().Be("Product");
            ((RedirectToRouteResult)result).RouteValues["Controller"].Should().Be("Admin");

            productRepository.VerifyAll();
        }

        [Test]
        public void When_requesting_the_admin_product_edit_image_page_result_is_correct()
        {
            const int expectedCategoryCount = 3;
            const string expectedViewName = "Category";
            const string productId = "2";
            var categoryRepository = new Mock<IRepository<Category>>();
            var productRepository = new Mock<IRepository<Product>>();

            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                      Id = "1"
                                                                  },
                                                              new Product
                                                                  {
                                                                      Id = productId
                                                                  },
                                                              new Product
                                                                  {
                                                                      Id = "3"
                                                                  }
                                                          });

            var controller = new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object);

            var result = controller.ProductImage(productId);
            result.AssertViewRendered().ForView("ProductImage");

            ((ViewResult)result).Model.Should().BeOfType<ProductViewModel>();
            ((ProductViewModel)((ViewResult)result).Model).Id.Should().Be(productId);
        }

        [Test]
        public void When_requesting_product_image_delete_image_is_removed_from_product()
        {
            const int expectedImageCount = 1;

            var productRepository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();

            var productId = Guid.NewGuid().ToString();
            var image1Id = Guid.NewGuid().ToString();
            var image2Id = Guid.NewGuid().ToString();

            productRepository.Setup(x => x.Save(It.IsAny<Product>())).Verifiable();

            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                      Id = productId,
                                                                      Images = new List<ImageInfo>
                                                                                   {
                                                                                       new ImageInfo
                                                                                           {
                                                                                                Id = image1Id,
                                                                                                MainImage = true,
                                                                                                OriginalFileName = string.Empty,
                                                                                                StoredFileName = string.Empty,
                                                                                                DisplayOrder = 0
                                                                                           },
                                                                                       new ImageInfo
                                                                                           {
                                                                                               Id = image2Id,
                                                                                                MainImage = true,
                                                                                                OriginalFileName = string.Empty,
                                                                                                StoredFileName = string.Empty,
                                                                                                DisplayOrder = 1
                                                                                           }
                                                                                   }
                                                                  }
                                                          }).Verifiable();

            new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object)
                .DeleteImage(new ProductImageViewModel
                {
                    Id = productId
                }, image1Id);

            productRepository.VerifyAll();
        }

        [Test]
        public void When_moving_an_image_up_in_the_list_repository_is_used()
        {
            var productRepository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();

            productRepository.Setup(x => x.Save(It.IsAny<Product>())).Verifiable();

            var productId = Guid.NewGuid().ToString();
            var image1Id = Guid.NewGuid().ToString();
            var image2Id = Guid.NewGuid().ToString();

            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                      Id = productId,
                                                                      Images = new List<ImageInfo>
                                                                                   {
                                                                                       new ImageInfo
                                                                                           {
                                                                                                Id = image1Id,
                                                                                                MainImage = true,
                                                                                                OriginalFileName = string.Empty,
                                                                                                StoredFileName = string.Empty,
                                                                                                DisplayOrder = 0
                                                                                           },
                                                                                       new ImageInfo
                                                                                           {
                                                                                               Id = image2Id,
                                                                                                MainImage = true,
                                                                                                OriginalFileName = string.Empty,
                                                                                                StoredFileName = string.Empty,
                                                                                                DisplayOrder = 1
                                                                                           }
                                                                                   }
                                                                  }
                                                          }).Verifiable();

            new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object)
                .ProductMoveUp(new ProductImageViewModel
                {
                    Id = productId
                }, image1Id);

            productRepository.VerifyAll();
        }

        [Test]
        public void When_moving_an_image_down_in_the_list_repository_is_used()
        {
            var productRepository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();

            productRepository.Setup(x => x.Save(It.IsAny<Product>())).Verifiable();

            var productId = Guid.NewGuid().ToString();
            var image1Id = Guid.NewGuid().ToString();
            var image2Id = Guid.NewGuid().ToString();

            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                          {
                                                              new Product
                                                                  {
                                                                      Id = productId,
                                                                      Images = new List<ImageInfo>
                                                                                   {
                                                                                       new ImageInfo
                                                                                           {
                                                                                                Id = image1Id,
                                                                                                MainImage = true,
                                                                                                OriginalFileName = string.Empty,
                                                                                                StoredFileName = string.Empty,
                                                                                                DisplayOrder = 0
                                                                                           },
                                                                                       new ImageInfo
                                                                                           {
                                                                                               Id = image2Id,
                                                                                                MainImage = true,
                                                                                                OriginalFileName = string.Empty,
                                                                                                StoredFileName = string.Empty,
                                                                                                DisplayOrder = 1
                                                                                           }
                                                                                   }
                                                                  }
                                                          }).Verifiable();

            new AdminController(categoryRepository.Object, productRepository.Object, new Mock<IImageUploadService>().Object)
                .ProductMoveDown(new ProductImageViewModel
                {
                    Id = productId
                }, image1Id);

            productRepository.VerifyAll();
        }
    }
}