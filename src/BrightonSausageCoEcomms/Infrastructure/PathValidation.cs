using System;
using System.IO;

namespace BrightonSausageCoEcomms.Infrastructure
{
    public static class PathValidation
    {
        public static string ValidatePath(string basePath, string mappedPath)
        {
            var valid = false;

            try
            {
                valid = Path.GetFullPath(mappedPath).StartsWith(Path.GetFullPath(basePath), StringComparison.OrdinalIgnoreCase);
            }
            catch
            {
                valid = false;
            }

            if (!valid)
            {
                throw new ArgumentException("Invalid path basePath: {0} mappedPath: {1}".With(basePath, mappedPath));
            }

            return mappedPath;
        }
    }
}