<h1>Building the www.brightonsausageco.com website - 30/10/2012</h1>

<p>Battery died on the train to Nottingham yesterday so I shall be starting back on it tonight! Looking back at my final sentence before the battery karked it I see: "Let's go and add the IRepository<T>.GetAll() method then"... I best look at the tests and make sure that's where I left off...</p>

<p>After having a ganders I can see that I have added a MongoInstaller and MongoCollectionComponentLoader (wholesale robbed from https://gist.github.com/2427676 barr a couple of minor mods) which allows me to inject a lazy loaded MongoCollection<T> into my repositories. My repository implementation now looks like:</p>

<script src="https://gist.github.com/3982488.js?file=gistfile1.cs"></script>

<p>And a test:</p>

<script src="https://gist.github.com/3982502.js?file=gistfile1.cs"></script>

<p>So, now we have the repository working with MongoDB (!!) and it's being injected into the controller so we can now mock and test and actually use MongoDb in our project. Now we need to go and modify the AdminControllerBehaviors test which checks the result of calling the Categories action so that we check to see if there is a view model returned which contains a list of categories. That test now looks like :</p>

<script src="https://gist.github.com/3982696.js?file=gistfile1.cs"></script>

<p>Now if we run the specflow file now we're moving ahead again except we fail at:</p>
<script src="https://gist.github.com/3982770.js?file=gistfile1.txt"></script>

<p>This is because even though I know we are grabbing the categories from the DB we never actually saved them because we never had a test that demanded it. Now we do so lets extend the repository to add some persistence:</p>

<script src="https://gist.github.com/3982962.js?file=gistfile1.cs"></script>

<p>Now we just need to go back and change the CategoriesAdd POST test so that we make sure we save the category into the DB:</p>

<script src="https://gist.github.com/3983076.js?file=gistfile1.cs"></script>

<p>I've also whizzed through and updated the test for the loading of the categories action so that we are asserting that we are returning some categories:</p>

<script src="https://gist.github.com/3983099.js?file=gistfile1.cs"></script>

<p>And the updated CategoriesController now looks like (I'm not suggesting this code is production ready but it passes this test!):

<script src="https://gist.github.com/3983108.js?file=gistfile1.cs"></script>

<p>I also added the following to the Categories view:</p>

<script src="https://gist.github.com/3983112.js?file=gistfile1.html"></script>

<p>Now that we have persistence using MongoDb (which I have never used before) and we have all of the IoC, MVC infrastructure stuff all set up and we can save categories I feel pretty good about the progress. Picking off the categories feature was going easy on myself because it was the easiest feature to drop into. But I also knew that this feature would also bare the brunt of the infrastructure bootstrapping side of stuff. Although this feature isn't 100% complete and the code that is there isn't exactly tight I am happy that it's a good grounding to build on. I'm going to leave it there tonight. Next session I need to complete the feature and make the specflow test pass.</p>