using System;
using System.Collections.Generic;

namespace BrightonSausageCoEcomms.Infrastructure
{
    public interface IRepository<T> : IDisposable
    {
        void Save(T doc);
        void Save(IEnumerable<T> docsToSave);
        IList<T> GetAll();
        void Delete(string id);
    }
}