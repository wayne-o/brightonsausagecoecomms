using System;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.Windsor;

namespace BrightonSausageCoEcomms
{
    public class WindsorControllerFactory : DefaultControllerFactory
    {
        private readonly IWindsorContainer container;

        public WindsorControllerFactory(IWindsorContainer container)
        {
            this.container = container;
        }

        public override void ReleaseController(IController controller)
        {
            container.Kernel.ReleaseComponent(controller);
        }

        protected override IController GetControllerInstance(RequestContext context, Type controllerType)
        {
            if (controllerType != null)
            {
                return (IController)container.Kernel.Resolve(controllerType);
            }
            return null;
        }
    }
}