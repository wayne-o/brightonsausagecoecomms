using System;
using System.Collections.Generic;

namespace BrightonSausageCoEcomms.Infrastructure
{
    public static class IEnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (T item in items)
            {
                action(item);
            }
        }
    }
}