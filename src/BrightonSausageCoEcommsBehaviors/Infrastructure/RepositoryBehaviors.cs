﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Installers;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Repository;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using FluentAssertions;
using NUnit.Framework;

namespace BrightonSausageCoEcommsBehaviors.Infrastructure
{
    [TestFixture]
    public class RepositoryBehaviors
    {
        [Test]
        public void When_fetching_all_documents_all_are_returned()
        {
            const int expectedCategoryCount = 2;

            var repository = GetRepository();

            repository.Save(new Category
                                {
                                    Id = "test1"
                                });
            repository.Save(new Category
                                {
                                    Id = "test2"
                                });

            var result = repository.GetAll();
            result.Count().Should().Be(expectedCategoryCount);
        }

        [Test]
        public void When_saving_a_new_document_document_is_persisted()
        {
            const int expectedCategoryCount = 2;
            var repository = GetRepository();

            repository.Save(new List<Category>
                                {
                                    new Category
                                        {
                                            Id = Guid.NewGuid().ToString()
                                        }, 
                                    new Category
                                        {
                                            Id = Guid.NewGuid().ToString()
                                        }
                                });

            var result = repository.GetAll();
            result.Count().Should().Be(expectedCategoryCount);
        }

        [TearDown]
        public void TearDown()
        {
            var repository = GetRepository();
            repository.GetAll().ForEach(x => repository.Delete(x.Id));
        }

        private static IRepository<Category> GetRepository()
        {
            var container = new WindsorContainer();
            container.Install(new MongoInstaller());

            container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)));

            var repository = container.Resolve<IRepository<Category>>();
            return repository;
        }
    }
}
