﻿Feature: Viewing Home Page
	In order view the websites home page
	As a standard user
	I want to be able to browse the home page

Scenario: Clicking the "Home" menu item takes me to the "Home" page
	Given I am on the Homepage 
	When I click the "Home" link 
	Then I expect to see the "Home" page

Scenario: Clicking the "About" menu item takes me to the "About" page
	Given I am on the Homepage 
	When I click the "About" link 
	Then I expect to see the "About" page

Scenario: Clicking the "Products" menu item takes me to the "Products" page
	Given I am on the Homepage 
	When I click the "Products" link 
	Then I expect to see the "Products" page

Scenario: Clicking the "Clients/Suppliers" menu item takes me to the "Clients/Suppliers" page
	Given I am on the Homepage 
	When I click the "Clients/Suppliers" link 
	Then I expect to see the "Clients/Suppliers" page

Scenario: Clicking the "Contact" menu item takes me to the "Contact" page
	Given I am on the Homepage 
	When I click the "Contact" link 
	Then I expect to see the "Contact" page
