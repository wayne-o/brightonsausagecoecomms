properties {
    $buildSuccessfulMessage = 'Solution Successfully Built!'
    $buildFailureMessage = 'Solution Failed to Build!'
    $cleanMessage = 'Executed Clean!'
	$build_number = if ("$env:BUILD_NUMBER".length -gt 0) { "$env:BUILD_NUMBER" } else { "0.0.0.1" }
	$project_dir = Split-Path $psake.build_script_file
	$BrightonSausageCoEcomms_solution = "$project_dir\BrightonSausageCoEcomms.sln"
	
	$build_artefacts_dir = "$project_dir\build\"
	$src_dir = "$project_dir\src"
	$lib_dir = "$project_dir\lib"
	
	$BrightonSausageCoEcomms_web = "$src_dir\BrightonSausageCoEcomms\BrightonSausageCoEcomms.csproj"
	
	$build_configuration = "Release"
	
	$nunit_runner = "$lib_dir\nunit.runners.2.6.1\tools\nunit-console.exe"
}

include psake_ext.ps1

task default -depends Build, RunUnitTests

task Clean {
     Write-Output "Running cleanup task"
 
	 if (Test-Path $build_artefacts_dir) {
	 	Remove-Item $build_artefacts_dir -rec -force 
	 }
	 md $build_artefacts_dir | Out-Null
	 md $build_artefacts_dir/reports | Out-Null
}
  
Task Version -Description "Version the assemblies" -depends Clean{
		Update-AssemblyInfoFiles $build_number ''
}

task Build -depends Version {

    Write-Output "Compiling solution"
	exec { msbuild $BrightonSausageCoEcomms_solution /p:Configuration=$build_configuration /p:OutDir=$build_artefacts_dir }
}


Task RunUnitTests {
	
	if (!(Test-Path -Path "$build_artefacts_dir/reports/unit.tests")) {
		md "$build_artefacts_dir/reports/unit.tests"
	}
	
	exec {& $nunit_runner "$build_artefacts_dir/BrightonSausageCoEcommsBehaviors.dll" /xml:"$build_artefacts_dir/reports/unit.tests/result.xml" /nologo }

}


function Update-AssemblyInfoFiles ([string] $version, [System.Array] $excludes = $null, $make_writeable = $false) {

#-------------------------------------------------------------------------------
# Update version numbers of AssemblyInfo.cs
# adapted from: http://www.luisrocha.net/2009/11/setting-assembly-version-with-windows.html
#-------------------------------------------------------------------------------

	if ($version -notmatch "[0-9]+(\.([0-9]+|\*)){1,3}") {
		Write-Error "Version number incorrect format: $version"
	}

	$versionPattern = 'AssemblyVersion\("[0-9]+(\.([0-9]+|\*)){1,3}"\)'
	$versionAssembly = 'AssemblyVersion("' + $version + '")';
	$versionFilePattern = 'AssemblyFileVersion\("[0-9]+(\.([0-9]+|\*)){1,3}"\)'
	$versionAssemblyFile = 'AssemblyFileVersion("' + $version + '")';

	Get-ChildItem -r -filter AssemblyInfo.cs | % {
		$filename = $_.fullname

		$update_assembly_and_file = $true

		# set an exclude flag where only AssemblyFileVersion is set
		if ($excludes -ne $null)
			{ $excludes | % { if ($filename -match $_) { $update_assembly_and_file = $false	} } }


		# We are using a source control (TFS) that requires to check-out files before 
		# modifying them. We don't want checkins so we'll just toggle
		# the file as writeable/readable	

		if ($make_writable) { Writeable-AssemblyInfoFile($filename) }

		# see http://stackoverflow.com/questions/3057673/powershell-locking-file
		# I am getting really funky locking issues. 
		# The code block below should be:
		#     (get-content $filename) | % {$_ -replace $versionPattern, $version } | set-content $filename

		$tmp = ($file + ".tmp")
		if (test-path ($tmp)) { remove-item $tmp }

		if ($update_assembly_and_file) {
			(get-content $filename) | % {$_ -replace $versionFilePattern, $versionAssemblyFile } | % {$_ -replace $versionPattern, $versionAssembly }  > $tmp
			write-host Updating file AssemblyInfo and AssemblyFileInfo: $filename --> $versionAssembly / $versionAssemblyFile
		} else {
			(get-content $filename) | % {$_ -replace $versionFilePattern, $versionAssemblyFile } > $tmp
			write-host Updating file AssemblyInfo only: $filename --> $versionAssemblyFile
		}

		if (test-path ($filename)) { remove-item $filename }
		move-item $tmp $filename -force	

		if ($make_writable) { ReadOnly-AssemblyInfoFile($filename) }		

	}
}

function Reset-AssemblyInfoFiles(){
	Update-AssemblyInfoFiles ("1.0.0.0")
}