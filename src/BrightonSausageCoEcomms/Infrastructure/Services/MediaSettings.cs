﻿using System.Collections.Generic;

namespace BrightonSausageCoEcomms.Infrastructure.Services
{
    public class MediaSettings : IMediaSettings
    {
        public List<string> ExtensionBlackList
        {
            get
            {
                return new List<string>
                           {
                               "exe"
                           };
            }
        }
    }
}