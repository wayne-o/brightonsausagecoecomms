﻿using System;
using System.Collections.Generic;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;
using FluentAssertions;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace BrightonSausageCoEcommsBehaviors.Specs
{
    [Binding]
    public class ViewingProductPageSteps : StepsBase
    {
        private const string ProductsHomePage = "http://localhost:8900/products";

        [Given(@"the following categories exist")]
        public void GivenTheFollowingCategoriesExist(Table table)
        {
            var categoryRepository = WindsorContainer.Resolve<IRepository<Category>>();

            var categories = table.CreateSet<Category>();

            foreach (var category in categories)
            {
                category.Slug = SlugConverter.TitleToSlug(category.Name);
                categoryRepository.Save(category);
            }
        }
        
        [Given(@"the following products exist")]
        public void GivenTheFollowingProductsExist(Table table)
        {
            var productsRepository = WindsorContainer.Resolve<IRepository<Product>>();

            var products = table.CreateSet<Product>();

            foreach (var product in products)
            {
                product.Id = Guid.NewGuid().ToString();
                product.Slug = SlugConverter.TitleToSlug(product.Name);
                product.Categories = new List<CategoryInfo>
                                         {
                                             new CategoryInfo
                                                 {
                                                     Enabled = true,
                                                     Featured = true,
                                                     Name = "Test Category"
                                                 } 
                                         };
                productsRepository.Save(product);
            }
        }
        
        [Given(@"I have navigated to the Products page")]
        public void GivenIHaveNavigatedToTheProductsPage()
        {
            BrowserSession.Visit(ProductsHomePage);
        }
        
        [Then(@"I expect to see the featured products listed")]
        public void ThenIExpectToSeeTheFeaturedProductsListed()
        {
            //ScenarioContext.Current.Pending();
        }

        [Then(@"I expect to see the product detail page for ""(.*)""")]
        public void ThenIExpectToSeeTheProductDetailPageFor(string p0)
        {
            BrowserSession.Title.Should().Be(p0);
        }


        [AfterScenario]
        public static void AfterScenario()
        {
            var productRepository = WindsorContainer.Resolve<IRepository<Product>>();

            foreach (var product in productRepository.GetAll())
            {
                productRepository.Delete(product.Id);
            }

            var categoryRepository = WindsorContainer.Resolve<IRepository<Category>>();

            foreach (Category category in categoryRepository.GetAll())
            {
                categoryRepository.Delete(category.Id);
            }
        }
    }
}
