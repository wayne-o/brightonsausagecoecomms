using System.Collections.Generic;
using System.Linq;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;

namespace BrightonSausageCoEcomms.Infrastructure.Models
{
    public class Product
    {
        public Product()
        {
            Images = new List<ImageInfo>();
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ShortDescription { get; set; }

        public bool Published { get; set; }

        public decimal Price { get; set; }

        public string Slug { get; set; }

        public string Id { get; set; }

        public IList<ImageInfo> Images { get; set; }

        public List<CategoryInfo> Categories { get; set; }

        public void DeleteImage(string imageId)
        {
            var imageToRemove = Images.First(x => x.Id == imageId);

            if (imageToRemove != null)
            {
                Images.Remove(imageToRemove);
            }
        }

        public void MoveImageUp(string imageId)
        {
            var imageToMove = Images.First(x => x.Id == imageId);
            if (imageToMove.DisplayOrder > Images.OrderBy(x=>x.DisplayOrder).Min(x=>x.DisplayOrder))
            {
                var imageToSwapWith = Images.First(x => x.DisplayOrder == (imageToMove.DisplayOrder - 1));

                imageToMove.DisplayOrder = imageToMove.DisplayOrder - 1;
                imageToSwapWith.DisplayOrder = imageToSwapWith.DisplayOrder + 1;
            }
        }

        public void MoveImageDown(string imageId)
        {
            var imageToMove = Images.First(x => x.Id == imageId);

            if (imageToMove.DisplayOrder < Images.Count -1)
            {
                var imageToSwapWith = Images.First(x => x.DisplayOrder == (imageToMove.DisplayOrder + 1));

                imageToMove.DisplayOrder = imageToMove.DisplayOrder + 1;
                imageToSwapWith.DisplayOrder = imageToSwapWith.DisplayOrder - 1;
            }
        }
    }
}