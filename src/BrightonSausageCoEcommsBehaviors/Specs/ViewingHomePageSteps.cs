﻿using System;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace BrightonSausageCoEcommsBehaviors.Specs
{
    [Binding]
    public class ViewingHomePageSteps : StepsBase
    {
        private const string HomePage = "http://localhost:8900";

        [Given(@"I am on the Homepage")]
        public void GivenIAmOnTheHomepage()
        {
            BrowserSession.Visit(HomePage);
        }
        
        [Then(@"I expect to see the ""(.*)"" page")]
        public void ThenIExpectToSeeThePage(string p0)
        {
            BrowserSession.Title.Should().Be(p0);
        }
    }
}
