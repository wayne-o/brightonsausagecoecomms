namespace BrightonSausageCoEcomms.ViewModels.Admin
{
    public class ItemViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
    }
}