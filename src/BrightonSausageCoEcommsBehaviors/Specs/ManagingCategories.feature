﻿Feature: Manage Product Categories
    In order to group products by categories
    As an admin of the site
    I want to be able to manage categories in the system

Background:
	Given I am an admin user and I have logged in
	And I have navigated to the manage categories screen

@UI
Scenario: adding an enabled category
	Given I click the "Add category" link
		And I enter "category1" into the "CategoryName" textbox
		And I ensure the "Enabled" checkbox is "true"
	When I click the "Save" button
	Then I should see "category1" show up in the list of existing categories
		And row for "category-category1" should be "enabled"

Scenario: adding an disabled category
	Given I click the "Add category" link
		And I enter "category1" into the "CategoryName" textbox
		And I ensure the "Enabled" checkbox is "false"
	When I click the "Save" button
	Then I should see "category1" show up in the list of existing categories
		And row for "category-category1" should be "disabled"

Scenario: adding a category with no name
	Given I click the "Add category" link
		And there are already 2 categories in the system
		And I enter "" into the "CategoryName" textbox
		And I ensure the "Enabled" checkbox is "true"
	When I click the "Save" button
	Then the error for field "CategoryName" should be "The CategoryName field is required."
		And there should be 2 category in the system
		And I should see the "Add Category" screen

Scenario: adding a category with a name that already exists
	Given there is already a category with a name of "category1"
		And I have navigated to the manage categories screen
		And I click the "Add category" link
		And I enter "category1" into the "CategoryName" textbox
		And I ensure the "Enabled" checkbox is "true"
	When I click the "Save" button
	Then the error for field "CategoryName" should be "A category with that name already exists"
		And there should be 1 category in the system 
		And I should see the "Add Category" screen

Scenario: Editing an existing category edit form should be pre-populated with selected category data
	Given there are already 30 categories in the system
		And I have navigated to the manage categories screen
	When I click the "Edit" link for the first category in the list
	Then I should see the "Add Category" screen
		And it should be pre-populated with the categories data

Scenario: Canceling out of the add category form
	Given I click the "Add category" link
	When I click the "Cancel" button
	Then I should see the "Add Category" screen
