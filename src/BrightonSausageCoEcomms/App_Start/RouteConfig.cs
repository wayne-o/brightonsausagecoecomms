﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BrightonSausageCoEcomms
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Default", "", new { controller = "Home", action = "Index" });

            routes.MapRoute("About", "About", new { controller = "About", action = "Index" });

            routes.MapRoute("Contact", "Contact", new { controller = "Contact", action = "Index" });

            routes.MapRoute("ClientsSuppliers", "Clients", new { controller = "ClientsSuppliers", action = "Index" });

            routes.MapRoute("AdminProductImage", "Admin/Product/Image/{productId}", new { controller = "Admin", action = "Productimage", productId = UrlParameter.Optional });

            routes.MapRoute("AdminCategoryEdit", "Admin/Category/Edit/{categoryId}", new { controller = "Admin", action = "CategoryEdit" });

            routes.MapRoute("Admin", "Admin", new { controller = "Admin", action = "Index" });

            routes.MapRoute("ProductsWithCategory", "Products/category/{categoryFilter}", new { controller = "Products", action = "Index", categoryFilter = UrlParameter.Optional });

            routes.MapRoute("ProductsWithDetails", "Products/details/{productSlug}", new { controller = "Products", action = "Product" });

            routes.MapRoute("Products", "Products", new { controller = "Products", action = "Index" });

            routes.MapRoute("AdminCategoryDelete", "Admin/Category/Delete/{categoryId}", new { controller = "Admin", action = "CategoryDelete" });

            routes.MapRoute("AdminProductDelete", "Admin/Product/Delete/{productId}", new { controller = "Admin", action = "ProductDelete" });

            routes.MapRoute("AdminCategory", "Admin/Category", new { controller = "Admin", action = "Category" });

            routes.MapRoute("AdminCategoryAdd", "Admin/Category/Add", new { controller = "Admin", action = "CategoryAdd" });

            routes.MapRoute("AdminCategoryEditPost", "Admin/Category/Edit", new { controller = "Admin", action = "CategoryEdit" });

            routes.MapRoute("AdminProduct", "Admin/Product", new { controller = "Admin", action = "Product" });

            routes.MapRoute("AdminProductAdd", "Admin/Product/Add", new { controller = "Admin", action = "ProductAdd" });

            routes.MapRoute("AdminProductEdit", "Admin/Product/Edit/{productId}", new { controller = "Admin", action = "ProductEdit", productId = UrlParameter.Optional });

            routes.MapRoute("AdminProductEditImage", "Admin/Product/Edit/Image/{productId}", new { controller = "Admin", action = "ProductImage" });

        }
    }
}