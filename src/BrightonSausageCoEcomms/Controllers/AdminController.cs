using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;
using BrightonSausageCoEcomms.Infrastructure.Services;
using BrightonSausageCoEcomms.ViewModels;
using BrightonSausageCoEcomms.ViewModels.Admin;

namespace BrightonSausageCoEcomms.Controllers
{
    public class AdminController : Controller
    {
        private readonly IRepository<Category> categoryRepository;
        private readonly IRepository<Product> productRepository;
        private readonly IImageUploadService imageUploadService;

        public AdminController(IRepository<Category> categoryRepository, IRepository<Product> productRepository, IImageUploadService imageUploadService)
        {
            this.categoryRepository = categoryRepository;
            this.productRepository = productRepository;
            this.imageUploadService = imageUploadService;
        }

        public ViewResult Category()
        {
            var categoriesListViewModel = new CategoryListViewModel(categoryRepository.GetAll());

            return View("Category", categoriesListViewModel);
        }

        [HttpGet]
        public ActionResult CategoryAdd()
        {
            return View("CategoryAdd");
        }

        [HttpPost]
        [ActionName("CategoryAdd")]
        [AcceptParameter(Action = "Save")]
        public ActionResult CategoryAdd(CategoryViewModel model)
        {
            CheckCategoryNameForUniqueness(model, false);

            if (ModelState.IsValid == false)
            {
                return View("CategoryAdd", model);
            }

            categoryRepository.Save(new Category
                                {
                                    Id = Guid.NewGuid().ToString(),
                                    Name = model.CategoryName,
                                    Enabled = model.Enabled,
                                    Featured = model.Featured,
                                    Slug = SlugConverter.TitleToSlug(model.CategoryName)
                                });

            return RedirectToAction("Category", "Admin");
        }

        [HttpPost]
        [ActionName("CategoryAdd")]
        [AcceptParameter(Action = "CategoryAddCancel")]
        public ActionResult CategoryAddCancel()
        {
            return RedirectToAction("Category", "Admin");
        }

        [HttpGet]
        public ActionResult CategoryEdit(string categoryId)
        {
            Category category = categoryRepository.GetAll().First(x => x.Id == categoryId);

            return View("CategoryEdit", new CategoryViewModel
                            {
                                CategoryName = category.Name,
                                Enabled = category.Enabled,
                                Slug = category.Slug,
                                Id = category.Id
                            });
        }

        [HttpPost]
        [ActionName("CategoryEdit")]
        [AcceptParameter(Action = "Save")]
        public ActionResult CategoryEdit(CategoryViewModel model)
        {
            CheckCategoryNameForUniqueness(model, true);

            if (ModelState.IsValid == false)
            {
                return View("CategoryEdit", model);
            }

            Category category = categoryRepository.GetAll().First(x => x.Id == model.Id);

            category.Enabled = model.Enabled;
            category.Featured = model.Featured;
            category.Name = model.CategoryName;
            category.Slug = SlugConverter.TitleToSlug(model.CategoryName);

            categoryRepository.Save(category);

            return RedirectToAction("Category", "Admin");
        }

        [HttpPost]
        [ActionName("CategoryEdit")]
        [AcceptParameter(Action = "CategoryEditCancel")]
        public ActionResult CategoryEditCancel()
        {
            return RedirectToAction("Category", "Admin");
        }

        [HttpGet]
        public ActionResult CategoryDelete(string categoryId)
        {
            categoryRepository.Delete(categoryId);
            return RedirectToAction("Category", "Admin");
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View("Index");
        }

        [HttpGet]
        public ViewResult Product()
        {
            var productViewModel = new ProductListViewModel(productRepository.GetAll());

            return View("Product", productViewModel);
        }

        [HttpGet]
        public ActionResult ProductAdd()
        {
            var categories = categoryRepository.GetAll().Where(x => x.Enabled);
            var categoryInfos = new List<ItemViewModel>();
            categories.ForEach(x => categoryInfos.Add(new ItemViewModel
                                                          {
                                                              Name = x.Name,
                                                              Id = x.Id,
                                                              Checked = false
                                                          }));

            return View("ProductAdd", new ProductViewModel
                                          {
                                              Id = Guid.NewGuid().ToString(),
                                              AvalailableCategories = categoryInfos
                                          });
        }

        [HttpPost]
        [ActionName("ProductAdd")]
        [AcceptParameter(Action = "Save")]
        public ActionResult ProductAdd(ProductViewModel model)
        {
            CheckProductNameForUniqueness(model, false);

            if (ModelState.IsValid == false)
            {
                return View("ProductAdd", model);
            }

            var realcategories =
                categoryRepository.GetAll()
                                  .Where(
                                      x =>
                                      model.AvalailableCategories.Where(c => c.Checked).Select(y => y.Id).Contains(x.Id));

            var categories = realcategories.Select(x => new CategoryInfo
            {
                Name = x.Name,
                Enabled = x.Enabled,
                Featured = x.Featured
            });

            productRepository.Save(new Product
                                       {
                                           Id = model.Id,
                                           Name = model.ProductName,
                                           Description = model.ProductDescription,
                                           ShortDescription = model.ProductShortDescription,
                                           Published = model.Published,
                                           Price = model.Price,
                                           Slug = SlugConverter.TitleToSlug(model.ProductName),
                                           Categories = categories.ToList()
                                       });

            return RedirectToAction("Product", "Admin");
        }

        [HttpPost]
        [ActionName("ProductAdd")]
        [AcceptParameter(Action = "ProductAddCancel")]
        public ActionResult ProductAddCancel(ProductViewModel model)
        {
            return RedirectToAction("Product", "Admin");
        }

        [HttpGet]
        public ActionResult ProductEdit(string productid)
        {
            var product = productRepository.GetAll().First(x => x.Id == productid);

            var categories = categoryRepository.GetAll().Where(x => x.Enabled);
            var categoryInfos = new List<ItemViewModel>();
            categories.ForEach(x => categoryInfos.Add(new ItemViewModel
            {
                Name = x.Name,
                Id = x.Id,
                Checked = product.Categories.Any(catinfo => catinfo.Name == x.Name)
            }));

            return View("ProductEdit", new ProductViewModel
            {
                ProductName = product.Name,
                Published = product.Published,
                Slug = product.Slug,
                Id = product.Id,
                Price = product.Price,
                ProductDescription = product.Description,
                ProductShortDescription = product.ShortDescription,
                AvalailableCategories = categoryInfos.ToList()
            });
        }

        [HttpGet]
        public ActionResult ProductDelete(string productid)
        {
            productRepository.Delete(productid);
            return RedirectToAction("Product", "Admin");
        }

        [HttpPost]
        [ActionName("ProductEdit")]
        [AcceptParameter(Action = "Save")]
        public ActionResult ProductEdit(ProductViewModel model)
        {
            CheckProductNameForUniqueness(model, true);

            if (ModelState.IsValid == false)
            {
                return View("ProductEdit", model);
            }

            var product = productRepository.GetAll().First(x => x.Id == model.Id);

            var realcategories =
               categoryRepository.GetAll()
                                 .Where(
                                     x =>
                                     model.AvalailableCategories.Where(c => c.Checked).Select(y => y.Id).Contains(x.Id));

            var categories = realcategories.Select(x => new CategoryInfo
            {
                Name = x.Name,
                Enabled = x.Enabled,
                Featured = x.Featured
            });

            product.Published = model.Published;
            product.Name = model.ProductName;
            product.Description = model.ProductDescription;
            product.ShortDescription = model.ProductShortDescription;
            product.Price = model.Price;
            product.Slug = SlugConverter.TitleToSlug(model.ProductName);
            product.Categories = categories.ToList();
            productRepository.Save(product);

            return RedirectToAction("Product", "Admin");
        }

        [HttpPost]
        [ActionName("ProductEdit")]
        [AcceptParameter(Action = "ProductEditCancel")]
        public ActionResult ProductEditCancel(ProductViewModel model)
        {
            return RedirectToAction("Product", "Admin");
        }

        [HttpGet]
        [ActionName("ProductImage")]
        public ActionResult ProductImage(string productId)
        {
            var product = productRepository.GetAll().First(x => x.Id == productId);

            return View("ProductImage", new ProductImageViewModel
            {
                ProductName = product.Name,
                Id = product.Id,
                Images = product.Images.ToList()
            });
        }

        [HttpPost]
        [ActionName("ProductImage")]
        [AcceptParameter(Action = "ProductImageUpload")]
        public ActionResult ProductImage(ProductImageViewModel model)
        {
            var info = imageUploadService.UploadMediaFile(ConfigurationManager.AppSettings["ImagesUpload"], model.ProductImage);

            var product = productRepository.GetAll().First(x => x.Id == model.Id);

            info.Id = Guid.NewGuid().ToString();
            info.DisplayOrder = product.Images.Count + 1;

            product.Images.Add(info);

            productRepository.Save(product);

            return View("ProductImage", new ProductImageViewModel
            {
                ProductName = product.Name,
                Id = product.Id,
                Images = product.Images.ToList()
            });
        }

        [HttpPost]
        [ActionName("ProductImage")]
        [AcceptParameter(Action = "ProductImageCancel")]
        public ActionResult ProductImageCancel()
        {
            return RedirectToAction("Product", "Admin");
        }

        [HttpPost]
        [ActionName("ProductImage")]
        [AcceptParameter(Action = "ProductImageDelete", ActionRegex = "ProductImageDelete:[a-zA-Z0-9]{1,4}")]
        [InjectIndexParameter(ActionRegex = "ProductImageDelete:[a-zA-Z0-9]{1,4}", TargetArgument = "id")]
        public ActionResult DeleteImage(ProductImageViewModel vm, string id)
        {
            var product = productRepository.GetAll().First(x => x.Id == vm.Id);

            imageUploadService.DeleteMediaFile(ConfigurationManager.AppSettings["ImagesUpload"], product.Images.First(x=>x.Id == id).StoredFileName);

            product.DeleteImage(id);

            productRepository.Save(product);

            return View("ProductImage", vm);
        }

        [HttpPost]
        [ActionName("ProductImage")]
        [AcceptParameter(Action = "ProductImageMoveUp", ActionRegex = "ProductImageMoveUp:[a-zA-Z0-9]{1,4}")]
        [InjectIndexParameter(ActionRegex = "ProductImageMoveUp:[a-zA-Z0-9]{1,4}", TargetArgument = "id")]
        public ActionResult ProductMoveUp(ProductImageViewModel vm, string id)
        {
            var product = productRepository.GetAll().First(x => x.Id == vm.Id);
            product.MoveImageUp(id);
            productRepository.Save(product);
            return RedirectToAction("ProductImage", new { productId = vm.Id });
        }

        [HttpPost]
        [ActionName("ProductImage")]
        [AcceptParameter(Action = "ProductImageMoveDown", ActionRegex = "ProductImageMoveDown:[a-zA-Z0-9]{1,4}")]
        [InjectIndexParameter(ActionRegex = "ProductImageMoveDown:[a-zA-Z0-9]{1,4}", TargetArgument = "id")]
        public ActionResult ProductMoveDown(ProductImageViewModel vm, string id)
        {
            var product = productRepository.GetAll().First(x => x.Id == vm.Id);
            product.MoveImageDown(id);
            productRepository.Save(product);
            return RedirectToAction("ProductImage", new { productId = vm.Id });
        }

        private void CheckProductNameForUniqueness(ProductViewModel model, bool allowForExisting)
        {
            var numberToAllowFor = allowForExisting ? 2 : 1;

            if (productRepository.GetAll().Count(x => x.Name == model.ProductName) >= numberToAllowFor)
            {
                ModelState.AddModelError("ProductName", "A product with that name already exists");
            }
        }

        private void CheckCategoryNameForUniqueness(CategoryViewModel model, bool allowForExisting)
        {
            var numberToAllowFor = allowForExisting ? 2 : 1;

            if (categoryRepository.GetAll().Count(x => x.Name == model.CategoryName) >= numberToAllowFor)
            {
                ModelState.AddModelError("CategoryName", "A category with that name already exists");
            }
        }
    }
}