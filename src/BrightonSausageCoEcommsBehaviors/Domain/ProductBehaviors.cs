﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;
using FluentAssertions;
using NUnit.Framework;

namespace BrightonSausageCoEcommsBehaviors.Domain
{
    [TestFixture]
    public class ProductBehaviors
    {
        [Test]
        public void When_moving_image_up_in_list_image_display_order_is_correct()
        {
            const int expectedOrder = 1;
            var image3Id = Guid.NewGuid().ToString();
            var image2Id = Guid.NewGuid().ToString();
            var image1Id = Guid.NewGuid().ToString();

            var product = new Product
                              {
                                  Images = new List<ImageInfo>
                                               {
                                                   new ImageInfo
                                                       {
                                                           DisplayOrder = 0,
                                                           Id = image1Id
                                                       },
                                                    new ImageInfo
                                                        {
                                                            DisplayOrder = 1,
                                                            Id = image2Id
                                                        },
                                                     new ImageInfo
                                                        {
                                                            DisplayOrder = 2,
                                                            Id = image3Id
                                                        }
                                               }
                              };

            product.MoveImageUp(image3Id);

            var result = product.Images.First(x => x.Id == image3Id).DisplayOrder;

            result.Should().Be(expectedOrder);
        }

        [Test]
        public void When_moving_highest_product_up_nothing_is_done()
        {
            const int expectedOrder = 0;
            var image3Id = Guid.NewGuid().ToString();
            var image2Id = Guid.NewGuid().ToString();
            var image1Id = Guid.NewGuid().ToString();

            var product = new Product
            {
                Images = new List<ImageInfo>
                                               {
                                                   new ImageInfo
                                                       {
                                                           DisplayOrder = 0,
                                                           Id = image1Id
                                                       },
                                                    new ImageInfo
                                                        {
                                                            DisplayOrder = 1,
                                                            Id = image2Id
                                                        },
                                                     new ImageInfo
                                                        {
                                                            DisplayOrder = 2,
                                                            Id = image3Id
                                                        }
                                               }
            };

            product.MoveImageUp(image1Id);

            var result = product.Images.First(x => x.Id == image1Id).DisplayOrder;

            result.Should().Be(expectedOrder);
        }

        [Test]
        public void When_moving_lowest_product_down_nothing_is_done()
        {
            const int expectedOrder = 2;
            var image3Id = Guid.NewGuid().ToString();
            var image2Id = Guid.NewGuid().ToString();
            var image1Id = Guid.NewGuid().ToString();

            var product = new Product
            {
                Images = new List<ImageInfo>
                                               {
                                                   new ImageInfo
                                                       {
                                                           DisplayOrder = 0,
                                                           Id = image1Id
                                                       },
                                                    new ImageInfo
                                                        {
                                                            DisplayOrder = 1,
                                                            Id = image2Id
                                                        },
                                                     new ImageInfo
                                                        {
                                                            DisplayOrder = 2,
                                                            Id = image3Id
                                                        }
                                               }
            };

            product.MoveImageDown(image3Id);

            var result = product.Images.First(x => x.Id == image3Id).DisplayOrder;

            result.Should().Be(expectedOrder);
        }

        [Test]
        public void When_moving_image_down_in_list_image_display_order_is_correct()
        {
            const int expectedOrder = 2;
            var image3Id = Guid.NewGuid().ToString();
            var image2Id = Guid.NewGuid().ToString();
            var image1Id = Guid.NewGuid().ToString();

            var product = new Product
            {
                Images = new List<ImageInfo>
                                               {
                                                   new ImageInfo
                                                       {
                                                           DisplayOrder = 0,
                                                           Id = image1Id
                                                       },
                                                    new ImageInfo
                                                        {
                                                            DisplayOrder = 1,
                                                            Id = image2Id
                                                        },
                                                     new ImageInfo
                                                        {
                                                            DisplayOrder = 2,
                                                            Id = image3Id
                                                        }
                                               }
            };

            product.MoveImageDown(image2Id);

            var result = product.Images.First(x => x.Id == image2Id).DisplayOrder;

            result.Should().Be(expectedOrder);
        }
    }
}
