﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrightonSausageCoEcomms.Infrastructure.Models;

namespace BrightonSausageCoEcomms.ViewModels.Products
{
    public class ProductsHomepageViewModel
    {
        public IList<Product> Products { get; set; }

        public IList<Category> Categories { get; set; }
    }
}