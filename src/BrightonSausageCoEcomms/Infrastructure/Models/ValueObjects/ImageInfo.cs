﻿namespace BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects
{
    public class ImageInfo
    {
        public string OriginalFileName { get; set; }

        public string StoredFileName { get; set; }

        public bool MainImage { get; set; }

        public string Id { get; set; }

        public int DisplayOrder { get; set; }
    }
}