﻿using System;
using System.Globalization;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Models;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace BrightonSausageCoEcommsBehaviors.Specs
{
    [Binding]
    public class ManageProductsSteps : StepsBase
    {
        private const string AdminHomePage = "http://localhost:8900/admin";
        private readonly string httpLocalhostAdminProducts = "{0}/product".With(AdminHomePage);

        [Given(@"I have navigated to the admin home page")]
        public void GivenIHaveNavigatedToTheAdminHomePage()
        {
            BrowserSession.Visit(AdminHomePage);
        }

        [Given(@"there is already a product with a name of ""(.*)""")]
        public void GivenThereIsAlreadyAProductWithANameOf(string p0)
        {
            var productRepository = WindsorContainer.Resolve<IRepository<Product>>();

            productRepository.Save(new Product
            {
                Published = true,
                Id = Guid.NewGuid().ToString(),
                Name = p0,
                Slug = SlugConverter.TitleToSlug(p0)
            });
        }


        [Given(@"there is a category in the system called ""(.*)""")]
        public void GivenThereIsACategoryInTheSystemCalled(string p0)
        {
            var categoryRepository = WindsorContainer.Resolve<IRepository<Category>>();

            categoryRepository.Save(new Category
            {
                Enabled = true,
                Id = Guid.NewGuid().ToString(),
                Name = p0,
                Slug = SlugConverter.TitleToSlug(p0)
            });
        }

        [When(@"I enter ""(.*)"" into the ""(.*)"" textbox")]
        public void WhenIEnterIntoTheTextbox(string p0, string p1)
        {
            BrowserSession.FillIn(p1).With(p0);
        }

        [When(@"the ""(.*)"" checkbox is ""(.*)""")]
        public void WhenTheCheckboxIs(string p0, bool p1)
        {
            if (p1)
            {
                BrowserSession.Check(p0);
            }
            else
            {
                BrowserSession.Uncheck(p0);
            }
        }

        [Then(@"I should see the ""(.*)"" page")]
        public void ThenIShouldSeeThePage(string p0)
        {
            BrowserSession.Title.Should().Be(p0);
        }

        [Then(@"I should see ""(.*)"" show up in the list of existing products")]
        public void ThenIShouldSeeShowUpInTheListOfExistingProducts(string p0)
        {
            BrowserSession.HasContent(p0).Should().BeTrue();
        }

        [Given(@"there are already (.*) products in the system")]
        public void GivenThereAreAlreadyProductsInTheSystem(int p0)
        {
            GenerateProducts("existing-prod", p0);
        }

        [Given(@"I have navigated to the manage products screen")]
        public void GivenIHaveNavigatedToTheManageProductsScreen()
        {
            BrowserSession.Visit(httpLocalhostAdminProducts);
        }

        [When(@"I click the ""(.*)"" link for the first product in the list")]
        public void WhenIClickTheLinkForTheFirstProductInTheList(string p0)
        {
            const string xpath = "/html/body/table/tbody/tr[1]/td[3]/a";
            BrowserSession.FindXPath(xpath).Click();
        }

        [Then(@"it should be pre-populated with the products data")]
        public void ThenItShouldBePre_populatedWithTheProductsData()
        {
            const string xpath = "/html/body/form/ul/li/input[@id='ProductName']";
            BrowserSession.FindXPath(xpath).Value.Should().NotBe(string.Empty);
        }

        [Given(@"I click the ""(.*)"" link for the first product in the list")]
        public void GivenIClickTheLinkForTheFirstProductInTheList(string p0)
        {
            const string xpath = "/html/body/div/div/table/tbody/tr/td[3]/a";
            BrowserSession.FindXPath(xpath).Click();
        }

        [When(@"I click the ""(.*)"" link")]
        public void WhenIClickTheLink(string p0)
        {
            BrowserSession.FindLink(p0).Click();
        }

        [Then(@"I expect to there to be (.*) image in the list")]
        public void ThenIExpectToThereToBeImageInTheList(int p0)
        {
            const string xpath = "/html/body/div/div/ul[2]/li[1]";
            BrowserSession.FindXPath(xpath).Value.Should().NotBe(string.Empty);
        }

        [When(@"I click the ""(.*)"" link for the (.*)st image in the list")]
        public void WhenIClickTheLinkForTheStImageInTheList(string p0, int p1)
        {
            const string xpath = "html/body/div[@id='maincontent']/div[@id='wrap']/form/ul[2]/li[3]/input";
            BrowserSession.FindXPath(xpath).Click();
        }

        [Then(@"I expect there to be no images")]
        public void ThenIExpectThereToBeNoImages()
        {
            BrowserSession.Has(BrowserSession.FindCss("product-image")).Should().BeFalse();
        }

        [When(@"I click the ""(.*)"" button for the (.*)rd image")]
        public void WhenIClickTheButtonForTheRdImage(string p0, int p1)
        {
            const string xpath = "html/body/div[@id='maincontent']/div[@id='wrap']/form/ul[2]/li[17]/input";
            BrowserSession.FindXPath(xpath).Click();
        }


        [Then(@"I expect the image to move up in the list")]
        public void ThenIExpectTheImageToMoveUpInTheList()
        {
            const string xpath = "html/body/div[@id='maincontent']/div[@id='wrap']/form/ul[2]/li[7]";
            BrowserSession.FindXPath(xpath).HasContent("img3.jpg");
        }

        [Then(@"I expect the image to move down in the list")]
        public void ThenIExpectTheImageToMoveDownInTheList()
        {
            const string xpath = "html/body/div[@id='maincontent']/div[@id='wrap']/form/ul[2]/li[13]";
            BrowserSession.FindXPath(xpath).HasContent("img3.jpg");
        }

        [AfterScenario]
        public static void AfterScenario()
        {
            var productRepository = WindsorContainer.Resolve<IRepository<Product>>();

            foreach (var product in productRepository.GetAll())
            {
                productRepository.Delete(product.Id);
            }

        }

        [AfterFeature()]
        public static void AfterFeature()
        {
            BrowserSession.Dispose();
        }

        private static void GenerateProducts(string p0, int numberToGenerate)
        {
            var productsRepository = WindsorContainer.Resolve<IRepository<Product>>();

            for (var i = 0; i < numberToGenerate; i++)
            {
                productsRepository.Save(new Product()
                {
                    Published = true,
                    Price = (decimal)1.1,
                    Id = Guid.NewGuid().ToString(),
                    Name = "{0} - {1}".With(p0, i.ToString(CultureInfo.InvariantCulture)),
                    Slug =
                        SlugConverter.TitleToSlug("{0} - {1}".With(p0, i.ToString(CultureInfo.InvariantCulture)))
                });
            }
        }
    }
}
