﻿Feature: Manage Products
    In order to sell products to customers
    As an admin of the site
    I want to be able to manage products within my site

Background:
	Given I am an admin user and I have logged in
	And I have navigated to the admin home page

Scenario: Adding a product with basic information
	Given there is a category in the system called "Category 1"
		And I click the "Products" link
		And I click the "Add Product" link
	Then I should see the "Add Product" page
	When I enter "Product 1" into the "ProductName" textbox
		And I enter "Product 1" into the "ProductDescription" textbox
		And I enter "Product 1" into the "ProductShortDescription" textbox
		And I enter "2.00" into the "Price" textbox
		And the "Published" checkbox is "true"
	When I click the "Save" button
	Then I should see "Product 1" show up in the list of existing products

Scenario: Adding a product with basic information but a name that already exists
	Given there is already a product with a name of "Product 1"
		And there is a category in the system called "Category 1"
		And I click the "Products" link
		And I click the "Add Product" link
	Then I should see the "Add Product" page
	When I enter "Product 1" into the "ProductName" textbox
		And I enter "Product 1" into the "ProductDescription" textbox
		And I enter "Product 1" into the "ProductShortDescription" textbox
		And I enter "2.00" into the "Price" textbox
		And the "Published" checkbox is "true"
	When I click the "Save" button
	Then the error for field "ProductName" should be "A product with that name already exists"
		And there should be 1 product in the system 
		And I should see the "Add Product" screen

Scenario: Adding a product with basic information but no name 
	Given there is already a product with a name of "Product 1"
		And there is a category in the system called "Category 1"
		And I click the "Products" link
		And I click the "Add Product" link
	Then I should see the "Add Product" page
	When I enter "" into the "ProductName" textbox
		And I enter "Product 1" into the "ProductDescription" textbox
		And I enter "Product 1" into the "ProductShortDescription" textbox
		And I enter "2.00" into the "Price" textbox
		And the "Published" checkbox is "true"
	When I click the "Save" button
	Then the error for field "ProductName" should be "The ProductName field is required."
		And there should be 1 product in the system 
		And I should see the "Add Product" screen

Scenario: Adding a product with basic information but no description 
	Given there is already a product with a name of "Product 1"
		And there is a category in the system called "Category 1"
		And I click the "Products" link
		And I click the "Add Product" link
	Then I should see the "Add Product" page
	When I enter "Some product" into the "ProductName" textbox
		And I enter "" into the "ProductDescription" textbox
		And I enter "Product 1" into the "ProductShortDescription" textbox
		And I enter "2.00" into the "Price" textbox
		And the "Published" checkbox is "true"
	When I click the "Save" button
	Then the error for field "ProductDescription" should be "The ProductDescription field is required."
		And there should be 1 product in the system 
		And I should see the "Add Product" screen
		 
@TODO
Scenario: Adding a product with basic information but no category selected 
	Given there is already a product with a name of "Product 1"
		And there is a category in the system called "Category 1"
		And I click the "Products" link
		And I click the "Add Product" link
	Then I should see the "Add Product" page
	When I enter "Some product" into the "ProductName" textbox
		And I enter "" into the "ProductDescription" textbox
		And I enter "Product 1" into the "ProductShortDescription" textbox
		And I enter "2.00" into the "Price" textbox
		And the "Published" checkbox is "true"
	When I click the "Save" button
	Then the error for field "ProductDescription" should be "The ProductDescription field is required."
		And there should be 1 product in the system 
		And I should see the "Add Product" screen

Scenario: Adding a product with basic information but no short description 
	Given there is already a product with a name of "Product 1"
		And there is a category in the system called "Category 1"
		And I click the "Products" link
		And I click the "Add Product" link
	Then I should see the "Add Product" page
	When I enter "Some product" into the "ProductName" textbox
		And I enter "sdsfsf" into the "ProductDescription" textbox
		And I enter "" into the "ProductShortDescription" textbox
		And I enter "2.00" into the "Price" textbox
		And the "Published" checkbox is "true"
	When I click the "Save" button
	Then the error for field "ProductShortDescription" should be "The ProductShortDescription field is required."
		And there should be 1 product in the system 
		And I should see the "Add Product" screen

Scenario: Editing an existing product edit form should be pre-populated with selected category data
	Given there are already 30 products in the system
		And I have navigated to the manage products screen
	When I click the "Edit" link for the first product in the list
	Then I should see the "Edit Product" screen
		And it should be pre-populated with the products data

Scenario: Canceling out of the add product form
	Given I click the "Products" link
	And I click the "Add Product" link
	When I click the "Cancel" button
	Then I should see the "Products" screen

Scenario: Uploading an image for a product
	Given there are already 30 products in the system
		And I have navigated to the manage products screen 
		And I click the "Edit" link for the first product in the list
	Then I should see the "Edit Product" screen
	When I click the "Manage images" link
	Then I should see the "Manage Images" screen
	When I enter "c:\temp\img.jpg" into the "ProductImage" textbox
		And I click the "Upload" button
	Then I expect to there to be 1 image in the list

Scenario: Deleting an image for a product
Given there are already 30 products in the system
		And I have navigated to the manage products screen 
		And I click the "Edit" link for the first product in the list
	Then I should see the "Edit Product" screen
	When I click the "Manage images" link
	Then I should see the "Manage Images" screen
	When I enter "c:\temp\img.jpg" into the "ProductImage" textbox
		And I click the "Upload" button
	When I click the "delete image" link for the 1st image in the list
	Then I expect there to be no images

Scenario: Moving an image up in the list
Given there are already 30 products in the system
		And I have navigated to the manage products screen 
		And I click the "Edit" link for the first product in the list
	Then I should see the "Edit Product" screen
	When I click the "Manage images" link
	Then I should see the "Manage Images" screen
	When I enter "c:\temp\img1.jpg" into the "ProductImage" textbox
		And I click the "Upload" button
	When I enter "c:\temp\img2.jpg" into the "ProductImage" textbox
		And I click the "Upload" button
	When I enter "c:\temp\img3.jpg" into the "ProductImage" textbox
		And I click the "Upload" button
	When I click the "Move up" button for the 3rd image
		Then I expect the image to move up in the list

Scenario: Moving an image down in the list
Given there are already 30 products in the system
		And I have navigated to the manage products screen 
		And I click the "Edit" link for the first product in the list
	Then I should see the "Edit Product" screen
	When I click the "Manage images" link
	Then I should see the "Manage Images" screen
	When I enter "c:\temp\img1.jpg" into the "ProductImage" textbox
		And I click the "Upload" button
	When I enter "c:\temp\img2.jpg" into the "ProductImage" textbox
		And I click the "Upload" button
	When I enter "c:\temp\img3.jpg" into the "ProductImage" textbox
		And I click the "Upload" button
	When I click the "Move down" button for the 3rd image
		Then I expect the image to move down in the list