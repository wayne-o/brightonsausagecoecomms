﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.ViewModels.Products;

namespace BrightonSausageCoEcomms.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IRepository<Category> categoryRepository;
        private readonly IRepository<Product> productRepository;

        public ProductsController(IRepository<Category> categoryRepository, IRepository<Product> productRepository)
        {
            this.categoryRepository = categoryRepository;
            this.productRepository = productRepository;
        }

        public ViewResult Index(string categoryFilter)
        {
            IEnumerable<Category> categories;

            if (categoryRepository.GetAll() != null && categoryRepository.GetAll().Any())
            {
                categories = categoryRepository.GetAll().Where(x => x.Enabled);
            }
            else
            {
                categories = new List<Category>();
            }

            IEnumerable<Product> products;

            if (string.IsNullOrEmpty(categoryFilter))
            {
                if (productRepository.GetAll() != null && productRepository.GetAll().Any())
                {
                    products = productRepository.GetAll()
                                                        .Where(x => x.Published && x.Categories.Any(c => c.Featured));
                }
                else
                {
                    products = new List<Product>();
                }
            }
            else
            {
                products = productRepository.GetAll()
                                            .Where(x => x.Published && x.Categories.Any(c => c.Name == categoryFilter));
            }

            return View("Index", new ProductsHomepageViewModel
                                     {
                                         Products = products.ToList(),
                                         Categories = categories.ToList()
                                     });
        }

        public ViewResult Product(string productSlug)
        {
            var categories = categoryRepository.GetAll().Where(x => x.Enabled);
            var product = productRepository.GetAll().First(x => x.Slug == productSlug);

            return View("Product", new ProductsDetailspageViewModel
                            {
                                Product = product,
                                Categories = categories
                            });
        }
    }
}