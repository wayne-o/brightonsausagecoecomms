﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BrightonSausageCoEcomms.Controllers;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;
using BrightonSausageCoEcomms.ViewModels.Products;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace BrightonSausageCoEcommsBehaviors.Controllers
{
    [TestFixture]
    public class ProductsControllerBehaviors
    {
        [Test]
        public void When_requesting_the_products_page_result_is_correct()
        {
            const string expectedViewName = "Index";
            var categoryRepository = new Mock<IRepository<Category>>();
            var productRepository = new Mock<IRepository<Product>>();
            const int expectedCategoryCount = 2;
            const int expectedProductsCount = 1;

            SetupCategoryRepository(categoryRepository);

            SetupProductRepository(productRepository);

            var controller = new ProductsController(categoryRepository.Object, productRepository.Object);
            var result = controller.Index(null);


            result.Should().BeAssignableTo<ActionResult>();
            result.ViewName.Should().Be(expectedViewName);

            ((ProductsHomepageViewModel)result.Model).Categories.Count.Should().Be(expectedCategoryCount);
            ((ProductsHomepageViewModel)result.Model).Products.Count.Should().Be(expectedProductsCount);
        }

        [Test]
        public void When_requesting_the_product_details_page_result_is_correct()
        {
            const string expectedViewName = "Product";
            const string product = "Product 1";
            var categoryRepository = new Mock<IRepository<Category>>();
            var productRepository = new Mock<IRepository<Product>>();
            SetupCategoryRepository(categoryRepository);
            SetupProductRepository(productRepository);

            var controller = new ProductsController(categoryRepository.Object, productRepository.Object);
            var productSlug = SlugConverter.TitleToSlug(product);
            var result = controller.Product(productSlug);


            result.Should().BeAssignableTo<ActionResult>();
            result.ViewName.Should().Be(expectedViewName);

            ((ProductsDetailspageViewModel)result.Model).Product.Name.Should().Be(product);
        }

        [Test]
        public void When_requesting_the_product_page_and_there_are_no_products_or_categories_no_exception_is_thrown()
        {
            Action test = () =>
                              {
                                  const string expectedViewName = "Index";
                                  var categoryRepository = new Mock<IRepository<Category>>();
                                  var productRepository = new Mock<IRepository<Product>>();
                                  const int expectedCategoryCount = 2;
                                  const int expectedProductsCount = 1;

                                  var controller = new ProductsController(categoryRepository.Object, productRepository.Object);
                                  var result = controller.Index(null);
                              };

            test.ShouldNotThrow<Exception>();
        }

        private static void SetupProductRepository(Mock<IRepository<Product>> productRepository)
        {
            productRepository.Setup(x => x.GetAll()).Returns(new List<Product>
                                                                 {
                                                                     new Product
                                                                         {
                                                                             Name = "Product 1",
                                                                             Id = Guid.NewGuid().ToString(),
                                                                             Slug = SlugConverter.TitleToSlug( "Product 1"),
                                                                             Published = true,
                                                                             Categories = new List<CategoryInfo>
                                                                                              {
                                                                                                  new CategoryInfo
                                                                                                      {
                                                                                                          Name = "test 1",
                                                                                                          Enabled = true,
                                                                                                          Featured = true
                                                                                                      },
                                                                                                  new CategoryInfo
                                                                                                      {
                                                                                                          Name = "test 2",
                                                                                                          Enabled = true,
                                                                                                          Featured = true
                                                                                                      }
                                                                                              }
                                                                         },
                                                                     new Product
                                                                         {
                                                                             Name = "Product 2",
                                                                             Id = Guid.NewGuid().ToString(),
                                                                             Published = true,
                                                                             Slug = SlugConverter.TitleToSlug( "Product 2"),
                                                                             Categories = new List<CategoryInfo>
                                                                                              {
                                                                                                  new CategoryInfo
                                                                                                      {
                                                                                                          Name = "test 1",
                                                                                                          Enabled = false,
                                                                                                          Featured = false
                                                                                                      },
                                                                                                  new CategoryInfo
                                                                                                      {
                                                                                                          Name = "test 2",
                                                                                                          Enabled = false,
                                                                                                          Featured = false
                                                                                                      }
                                                                                              }
                                                                         }
                                                                 });
        }

        private static void SetupCategoryRepository(Mock<IRepository<Category>> categoryRepository)
        {
            categoryRepository.Setup(x => x.GetAll()).Returns(new List<Category>
                                                                  {
                                                                      new Category
                                                                          {
                                                                              Enabled = true,
                                                                              Id = Guid.NewGuid().ToString(),
                                                                              Name = "test 1",
                                                                              Featured = true
                                                                          },
                                                                      new Category
                                                                          {
                                                                              Enabled = true,
                                                                              Id = Guid.NewGuid().ToString(),
                                                                              Name = "test 2",
                                                                              Featured = true
                                                                          }
                                                                  });
        }
    }
}