﻿using System.Collections.Generic;
using BrightonSausageCoEcomms.Infrastructure.Models;

namespace BrightonSausageCoEcomms.ViewModels.Products
{
    public class ProductsDetailspageViewModel
    {
        public Product Product { get; set; }

        public IEnumerable<Category> Categories { get; set; }
    }
}