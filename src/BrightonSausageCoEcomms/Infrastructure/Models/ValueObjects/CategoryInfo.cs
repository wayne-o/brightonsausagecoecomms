namespace BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects
{
    public class CategoryInfo
    {
        public string Name { get; set; }

        public bool Enabled { get; set; }

        public bool Featured { get; set; }
    }
}