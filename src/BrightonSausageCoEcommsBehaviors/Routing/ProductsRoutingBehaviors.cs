﻿using System.Web.Routing;
using BrightonSausageCoEcomms;
using BrightonSausageCoEcomms.Controllers;
using MvcContrib.TestHelper;
using NUnit.Framework;

namespace BrightonSausageCoEcommsBehaviors.Routing
{
    [TestFixture]
    public class ProductsRoutingBehaviors
    {
        [SetUp]
        public void SetUp()
        {
            RouteCollection routes = RouteTable.Routes;
            routes.Clear();
            RouteConfig.RegisterRoutes(routes);
        }

        [Test]
        public void When_navigating_to_products_url_result_is_correct()
        {
            const string url = "~/products";
            url.ShouldMapTo<ProductsController>(x => x.Index(null));
        }

        [Test]
        public void When_navigating_to_products_with_category_filter_url_result_is_correct()
        {
            const string url = "~/products/category/sausages";
            url.ShouldMapTo<ProductsController>(x => x.Index("sausages"));
        }

        [Test]
        public void When_navigating_to_products_with_detail_filter_url_result_is_correct()
        {
            const string url = "~/products/details/sausage-1";
            url.ShouldMapTo<ProductsController>(x => x.Product("sausage-1"));
        }

        [Test]
        public void When_navigating_to_product_url_result_is_correct()
        {
            const string url = "~/Products/details/sausage-1";
            url.ShouldMapTo<ProductsController>(x => x.Product("sausage-1"));
        }
    }
}