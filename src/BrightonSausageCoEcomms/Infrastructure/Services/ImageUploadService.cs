﻿using System;
using System.IO;
using System.Web;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;

namespace BrightonSausageCoEcomms.Infrastructure.Services
{
    public class ImageUploadService : IImageUploadService
    {
        private readonly IFileSystemService fileService;
        private readonly IMediaSettings mediaSettings;

        public ImageUploadService(IFileSystemService fileService, IMediaSettings mediaSettings)
        {
            this.fileService = fileService;
            this.mediaSettings = mediaSettings;
        }

        public ImageInfo UploadMediaFile(string storagePath, HttpPostedFileBase file)
        {
            if (mediaSettings.ExtensionBlackList.Contains(GetExtension(file.FileName)))
            {
                throw new FileExtensionBlacklistValidationException();
            }

            var mappedpath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, storagePath);

            if (!fileService.FolderExists(mappedpath))
            {
                fileService.CreateFolder(mappedpath);
            }

            var newFilename = "{0}.{1}".With(Guid.NewGuid(), GetExtension(file.FileName));

            var savePath = Path.Combine(mappedpath, newFilename);

            file.SaveAs(savePath);

            return new ImageInfo { OriginalFileName = file.FileName, StoredFileName = newFilename };
        }

        public void DeleteMediaFile(string folderPath, string fileName)
        {
            fileService.DeleteFile(Path.Combine(folderPath, fileName));
        }

        private static string GetExtension(string fileName)
        {
            var extension = Path.GetExtension(fileName);
            return extension != null ? extension.Trim().TrimStart('.') : null;
        }
    }
}