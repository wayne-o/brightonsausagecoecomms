namespace BrightonSausageCoEcomms.Infrastructure
{
    public static class ObjectExtensions
    {
        public static TTo As<TTo>(this object subject)
        {
            return subject is TTo ? (TTo)subject : default(TTo);
        }
    }
}