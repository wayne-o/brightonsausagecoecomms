using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers;
using MongoDB.Driver;

namespace BrightonSausageCoEcomms.Infrastructure.Installers
{
    public class MongoCollectionComponentLoader : ILazyComponentLoader
    {
        readonly ConcurrentDictionary<Type, MethodInfo> factoryMethodCache = new ConcurrentDictionary<Type, MethodInfo>();

        public IRegistration Load(string key, Type service, IDictionary arguments)
        {
            var requestedServiceIsMongoCollection = service.IsGenericType
                                                    && service.GetGenericTypeDefinition() == typeof(MongoCollection<>);

            return !requestedServiceIsMongoCollection ? null : Component.For(service).UsingFactoryMethod(k => ResolveCollection(k, service));
        }

        object ResolveCollection(IKernel kernel, Type service)
        {
            var documentType = service.GetGenericArguments().Single();
            var mongoDatabase = kernel.Resolve<MongoDatabase>();

            var collectionMethodInfo = GetCollectionGetter(mongoDatabase, documentType);

            return collectionMethodInfo.Invoke(mongoDatabase, new object[] { documentType.Name });
        }

        MethodInfo GetCollectionGetter(MongoDatabase mongoDatabase, Type documentType)
        {
            MethodInfo methodInfoToReturn;

            if (!factoryMethodCache.TryGetValue(documentType, out methodInfoToReturn))
            {
                methodInfoToReturn = mongoDatabase.GetType()
                    .GetMethods()
                    .Single(m => m.Name == "GetCollection"
                                 && m.IsGenericMethod
                                 && m.GetParameters().Length == 1
                                 && m.GetParameters().Single().ParameterType == typeof(string))
                    .MakeGenericMethod(documentType);

                factoryMethodCache.TryAdd(documentType, methodInfoToReturn);
            }

            return methodInfoToReturn;
        }
    }
}