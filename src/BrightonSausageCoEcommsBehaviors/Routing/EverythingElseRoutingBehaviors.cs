﻿using System.Web.Routing;
using BrightonSausageCoEcomms;
using BrightonSausageCoEcomms.Controllers;
using MvcContrib.TestHelper;
using NUnit.Framework;

namespace BrightonSausageCoEcommsBehaviors.Routing
{
    [TestFixture]
    public class EverythingElseRoutingBehaviors
    {
        [SetUp]
        public void SetUp()
        {
            RouteCollection routes = RouteTable.Routes;
            routes.Clear();
            RouteConfig.RegisterRoutes(routes);
        }

        [Test]
        public void When_navigating_to_home_url_result_is_correct()
        {
            const string url = "~/";
            url.ShouldMapTo<HomeController>(x => x.Index());
        }

        [Test]
        public void When_navigating_to_contact_url_result_is_correct()
        {
            const string url = "~/contact";
            url.ShouldMapTo<ContactController>(x => x.Index());
        }

        [Test]
        public void When_navigating_to_about_url_result_is_correct()
        {
            const string url = "~/about";
            url.ShouldMapTo<AboutController>(x => x.Index());
        }

        [Test]
        public void When_navigating_to_clients_url_result_is_correct()
        {
            const string url = "~/clients";
            url.ShouldMapTo<ClientsSuppliersController>(x => x.Index());
        }
    }
}