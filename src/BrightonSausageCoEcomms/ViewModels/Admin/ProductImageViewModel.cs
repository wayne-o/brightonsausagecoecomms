using System.Collections.Generic;
using System.Web;
using BrightonSausageCoEcomms.Infrastructure.Models;
using BrightonSausageCoEcomms.Infrastructure.Models.ValueObjects;

namespace BrightonSausageCoEcomms.ViewModels.Admin
{
    public class ProductImageViewModel
    {
        public ProductImageViewModel()
        {
            Images = new List<ImageInfo>();
        }

        public string Id { get; set; }

        public string ProductName { get; set; }

        public HttpPostedFileBase ProductImage { get; set; }

        public List<ImageInfo> Images { get; set; }
    }
}