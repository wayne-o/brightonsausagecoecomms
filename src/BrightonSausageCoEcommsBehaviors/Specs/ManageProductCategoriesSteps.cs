﻿using System;
using System.Globalization;
using BrightonSausageCoEcomms.Infrastructure;
using BrightonSausageCoEcomms.Infrastructure.Models;
using Coypu;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace BrightonSausageCoEcommsBehaviors.Specs
{
    [Binding]
    public class ManageProductCategoriesSteps : StepsBase
    {
        private const string HttpLocalhostAdminCategories = "http://localhost:8900/admin/category";

        [Given(@"I am an admin user and I have logged in")]
        public void GivenIAmAnAdminUserAndIHaveLoggedIn()
        {
            //todo: come back to this
        }

        [Given(@"I have navigated to the manage categories screen")]
        public void GivenIHaveNavigatedToTheManageCategoriesScreen()
        {
            BrowserSession.Visit(HttpLocalhostAdminCategories);
        }

        [Given(@"I click the ""(.*)"" link")]
        public void GivenIClickTheLink(string p0)
        {
            BrowserSession.ClickLink(p0);
        }

        [Given(@"I enter ""(.*)"" into the ""(.*)"" textbox")]
        public void GivenIEnterIntoTheTextbox(string p0, string p1)
        {
            BrowserSession.FillIn(p1).With(p0);
        }

        [Given(@"I ensure the ""(.*)"" checkbox is ""(.*)""")]
        public void GivenIEnsureTheCheckboxIs(string p0, bool p1)
        {
            if (p1)
            {
                BrowserSession.Check(p0);
            }
            else
            {
                BrowserSession.Uncheck(p0);
            }
        }

        [When(@"I click the ""(.*)"" button")]
        public void WhenIClickTheButton(string p0)
        {
            BrowserSession.ClickButton(p0);
        }

        [Then(@"I should see ""(.*)"" show up in the list of existing categories")]
        public void ThenIShouldSeeShowUpInTheListOfExistingCategories(string p0)
        {
            BrowserSession.HasContent(p0).Should().BeTrue();
        }

        [Given(@"there are already (.*) categories in the system")]
        public void GivenThereAreAlreadyCategoriesInTheSystem(int p0)
        {
            GenerateCategories("existing-cat", p0);
        }

        [When(@"I click the ""(.*)"" link for the first category in the list")]
        public void WhenIClickTheLinkForTheFirstCategoryInTheList(string p0)
        {
            const string xpath = "/html/body/table/tbody/tr[1]/td[3]/a";
            BrowserSession.FindXPath(xpath).Click();
        }

        [Then(@"it should be pre-populated with the categories data")]
        public void ThenItShouldBePre_populatedWithTheCategoriesData()
        {
            const string xpath = "/html/body/form/ul/li/input[@id='CategoryName']";
            BrowserSession.FindXPath(xpath).Value.Should().NotBe(string.Empty);
        }

        [Then(@"row for ""(.*)"" should be ""(.*)""")]
        public void ThenRowForShouldBe(string p0, string p1)
        {
            const string xpath = "html/body/table/tbody/tr[@id='{0}']/td[@class='{1}']";

            ElementScope content = BrowserSession.FindXPath(xpath.With(p0, p1));

            content.Text.Should().NotBe(string.Empty);
        }

        [Then(@"there should be (.*) category in the system")]
        public void ThenThereShouldBeCategoryInTheSystem(int p0)
        {
            int expectedNumberOfCategories = p0;
            var categoryRepository = WindsorContainer.Resolve<IRepository<Category>>();

            categoryRepository.GetAll().Count.Should().Be(expectedNumberOfCategories);
        }

        [Then(@"I should see the ""(.*)"" screen")]
        public void ThenIShouldSeeTheScreen(string p0)
        {
            BrowserSession.Title.Should().Be(p0);
        }

        [Given(@"there is already a category with a name of ""(.*)""")]
        public void GivenThereIsAlreadyACategoryWithANameOf(string p0)
        {
            var categoryRepository = WindsorContainer.Resolve<IRepository<Category>>();

            categoryRepository.Save(new Category
            {
                Enabled = true,
                Id = Guid.NewGuid().ToString(),
                Name = p0,
                Slug = SlugConverter.TitleToSlug(p0)
            });
        }

        [Then(@"the error for field ""(.*)"" should be ""(.*)""")]
        public void ThenTheErrorForFieldShouldBe(string p0, string p1)
        {
            const string xpath = "/html/body/form/ul/li/span[@data-valmsg-for='{0}']";

            ElementScope content = BrowserSession.FindXPath(xpath.With(p0));

            content.Text.Should().Be(p1);
        }

        [Then(@"there should be (.*) product in the system")]
        public void ThenThereShouldBeProductInTheSystem(int p0)
        {
            int expectedNumberOfProducts = p0;
            var productRepository = WindsorContainer.Resolve<IRepository<Product>>();

            productRepository.GetAll().Count.Should().Be(expectedNumberOfProducts);
        }


        [AfterScenario]
        public static void AfterScenario()
        {
            var categoryRepository = WindsorContainer.Resolve<IRepository<Category>>();

            foreach (Category category in categoryRepository.GetAll())
            {
                categoryRepository.Delete(category.Id);
            }
        }

        private static void GenerateCategories(string p0, int numberToGenerate)
        {
            var categoryRepository = WindsorContainer.Resolve<IRepository<Category>>();

            for (int i = 0; i < numberToGenerate; i++)
            {
                categoryRepository.Save(new Category
                                            {
                                                Enabled = true,
                                                Id = Guid.NewGuid().ToString(),
                                                Name = "{0} - {1}".With(p0, i.ToString(CultureInfo.InvariantCulture)),
                                                Slug =
                                                    SlugConverter.TitleToSlug("{0} - {1}".With(p0,
                                                                                               i.ToString(
                                                                                                   CultureInfo.
                                                                                                       InvariantCulture)))
                                            });
            }
        }
    }
}