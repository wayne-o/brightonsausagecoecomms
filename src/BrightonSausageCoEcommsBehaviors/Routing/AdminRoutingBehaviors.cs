﻿using System.Web.Routing;
using BrightonSausageCoEcomms;
using BrightonSausageCoEcomms.Controllers;
using MvcContrib.TestHelper;
using NUnit.Framework;

namespace BrightonSausageCoEcommsBehaviors.Routing
{
    [TestFixture]
    public class AdminRoutingBehaviors
    {
        #region Setup/Teardown

        [SetUp]
        public void SetUp()
        {
            RouteCollection routes = RouteTable.Routes;
            routes.Clear();
            RouteConfig.RegisterRoutes(routes);
        }

        #endregion

        [Test]
        public void When_navigating_to_manage_categories_add_url_result_is_correct()
        {
            const string url = "~/admin/category/add";
            url.ShouldMapTo<AdminController>(x => x.CategoryAdd(null));
        }

        [Test]
        public void When_navigating_to_manage_categories_edit_url_result_is_correct()
        {
            const string url = "~/admin/category/edit/1";
            url.ShouldMapTo<AdminController>(x => x.CategoryEdit("1"));
        }

        [Test]
        public void When_navigating_to_manage_categories_url_result_is_correct()
        {
            const string url = "~/admin/category";
            url.ShouldMapTo<AdminController>(x => x.Category());
        }

        [Test]
        public void When_navigating_to_admin_url_result_is_correct()
        {
            const string url = "~/admin";
            url.ShouldMapTo<AdminController>(x => x.Index());
        }

        [Test]
        public void When_navigating_to_admin_product_url_result_is_correct()
        {
            const string url = "~/admin/product";
            url.ShouldMapTo<AdminController>(x => x.Product());
        }

        [Test]
        public void When_navigating_to_manage_product_add_url_result_is_correct()
        {
            const string url = "~/admin/product/add";
            url.ShouldMapTo<AdminController>(x => x.ProductAdd());
        }

        [Test]
        public void When_navigating_to_manage_product_edit_url_result_is_correct()
        {
            const string url = "~/admin/product/edit/1";
            url.ShouldMapTo<AdminController>(x => x.ProductEdit("1"));
        }

        [Test]
        public void When_navigating_to_manage_product_edit_image_url_result_is_correct()
        {
            const string url = "~/admin/product/image/1";
            url.ShouldMapTo<AdminController>(x => x.ProductImage("1"));
        }
    }
}